//
//  PT_FacebookClass.h
//  ProTeen
//
//  Created by Bhuriyo Patel on 02/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT_ImportClasses.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@class PT_GlobalMethods;

@interface PT_FacebookClass : NSObject
{
    //Facebook Login
    
    UIViewController *viewCtr;
    UIButton *buttonIndicatorObj;
}
@property (nonatomic, retain) ACAccountStore *accountStore;
@property (nonatomic, retain) ACAccount *facebookAccount;

- (void)loginWithViewCtr:(UIViewController*)vCtr indicatorButton:(UIButton *)btnIndicator withIndicatorText:(NSString*)strIndText withCompletionHandler:(void (^)(NSDictionary *Dic))completion;
+ (NSMutableDictionary *)setLoginWithFacebookWebServiceParameter:(NSDictionary*)dicFB;

- (void)getFBFriendsWithViewCtr:(UIViewController*)vCtr withCompletionHandler:(void (^)(NSDictionary *Dic))completion;

@end
