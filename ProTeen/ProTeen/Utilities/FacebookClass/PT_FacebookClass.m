//
//  PT_FacebookClass.m
//  ProTeen
//
//  Created by Bhuriyo Patel on 02/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_FacebookClass.h"
#import "PT_ImportClasses.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@implementation PT_FacebookClass

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
    }
    return self;
}


@synthesize accountStore,facebookAccount;

- (void)loginWithViewCtr:(UIViewController*)vCtr indicatorButton:(UIButton *)btnIndicator withIndicatorText:(NSString*)strIndText withCompletionHandler:(void (^)(NSDictionary *Dic))completion
{
    
    btnIndicator.useActivityIndicator=YES;
    [btnIndicator activityIndicatorVisibility:YES withViewController:vCtr withTitle:@"" withColor:[UIColor whiteColor]];
    
    viewCtr = vCtr;
    if ([PT_GlobalMethods checkConnection:nil])
    {        
        self.accountStore = [[ACAccountStore alloc] init];
        
        NSString *key = FACEBOOK_APPID;
        
        ACAccountType *FBaccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        if (self.accountStore && FBaccountType)
        {
            NSArray *arryPermission = [NSArray arrayWithObjects:@"email",@"public_profile", nil];
            NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,arryPermission,ACFacebookPermissionsKey, nil];
            
            [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
             ^(BOOL granted, NSError *e) {
                 
                 if (e)
                 {
                     [self performSelectorOnMainThread:@selector(showAlertWithSettingDevice) withObject:nil waitUntilDone:YES];
                 }
                 else if (granted)
                 {
                     NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
                     self.facebookAccount = [accounts lastObject];
                     
                     NSLog(@"%@",[self.facebookAccount username]);

//                     NSLog(@"%@",[self.facebookAccount ];
                     
                     
                     
                     [self getFBmewithCompletionMethod:^(NSDictionary *Dic)
                      {
                          dispatch_async(dispatch_get_main_queue(), ^(){
                              [btnIndicator activityIndicatorVisibility:NO withViewController:vCtr withTitle:@"Sign In" withColor:[UIColor whiteColor]];
                          });
                          
                        if (completion)
                              completion(Dic);
                      }];
                 }
                 else if (!granted)
                 {
                     [self performSelectorOnMainThread:@selector(showAlertwithNoGranted) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
                     [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:YES];
                 }
             }];
        }
        else {
            [self performSelectorOnMainThread:@selector(showAlert) withObject:nil waitUntilDone:YES];
        }
    }
    else
    {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"strNoInternetMessage", nil)];
    }
    
}

- (void)getFBmewithCompletionMethod:(void (^)(NSDictionary *Dic))completion
{
    NSString *acessToken = [NSString stringWithFormat:@"%@",self.facebookAccount.credential.oauthToken];
    NSDictionary *param = @{@"access_token": acessToken};
    
    NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                              requestMethod:SLRequestMethodGET
                                                        URL:meurl
                                                 parameters:param];
    
    merequest.account = self.facebookAccount;
    [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if (error)
        {
            [self performSelectorOnMainThread:@selector(showAlertwithError:) withObject:error waitUntilDone:YES];
        }
        else if (responseData)
        {
            NSError *errorMe = nil;
            NSMutableDictionary  *dict = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&errorMe];
            
            if (errorMe)
            {
                [self performSelectorOnMainThread:@selector(showAlertwithError:) withObject:errorMe waitUntilDone:YES];
                
            }
            else if ([dict objectForKey:@"id"])
            {
                if (completion)
                {
                    completion(dict);
                }
            }
            else if ([dict objectForKey:@"error"])
            {
                NSString *strCode = [NSString stringWithFormat:@"%@",dict[@"error"][@"code"]];
                if ([strCode isEqualToString:@"190"])
                {
                    [self performSelectorOnMainThread:@selector(appPermissionAlert) withObject:nil waitUntilDone:YES];
                }
                else
                {
                    [self performSelectorOnMainThread:@selector(showAlertFromFBError:) withObject:[dict objectForKey:@"error"] waitUntilDone:YES];
                }
            }
            else
            {
                if (completion) {
                    completion(nil);
                }
            }
        }
        
    }];
}

- (void)showAlert
{
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"strFacebookFailureMessage", nil)];
}

- (void)showAlertwithError:(NSError*)e
{
    [SVProgressHUD showErrorWithStatus:[e localizedDescription]];
}

- (void)showAlertwithNoGranted
{
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"strPermissionNotGrantedMessage", nil)];
}

- (void)showAlertWithSettingDevice
{
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"strNoFacebookAccountMessage", nil)];
}

- (void)showAlertFromFBError:(NSDictionary*)dic
{
    [SVProgressHUD showErrorWithStatus:dic[@"message"]];
}

- (void)appPermissionAlert
{
    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"strPermissionAlertMessage", nil)];
}

- (void)getFBFriendsWithViewCtr:(UIViewController*)vCtr withCompletionHandler:(void (^)(NSDictionary *Dic))completion
{
    if ([PT_GlobalMethods checkConnection:nil])
    {
        viewCtr = vCtr;
        self.accountStore = [[ACAccountStore alloc] init];
        
        NSString *key = FACEBOOK_APPID;
        
        ACAccountType *FBaccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        
        if (self.accountStore && FBaccountType)
        {
            NSArray *arryPermission = [NSArray arrayWithObjects:@"email", nil];
            NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,arryPermission,ACFacebookPermissionsKey, nil];
            
            [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
             ^(BOOL granted, NSError *e) {
                 
                 if (granted && !e)
                 {
                     NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
                     self.facebookAccount = [accounts lastObject];
                     NSString *acessToken = [NSString stringWithFormat:@"%@",facebookAccount.credential.oauthToken];
                     NSDictionary *parameters = @{@"access_token": acessToken,@"fields":@"id"};
                     
                     NSString *url = [NSString stringWithFormat:@"https://graph.facebook.com/me/friends"];
                     
                     NSURL *requestURL = [NSURL URLWithString:url];
                     
                     SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:requestURL parameters:parameters];
                     request.account = self.facebookAccount;
                     
                     [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
                         
                         if(!error)
                         {
                             NSDictionary *friendslist =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                             if (completion)
                                 completion(friendslist);
                         }
                         else
                         {
                             if (completion)
                                 completion(nil);
                         }
                     }];
                     
                 }
                 else
                 {
                     if (completion)
                         completion(nil);
                 }
             }];
        }
        else
        {
            if (completion)
                completion(nil);
        }
    }
    else
    {
        if (completion)
            completion(nil);
    }
}


+ (NSMutableDictionary *)setLoginWithFacebookWebServiceParameter:(NSDictionary*)dicFB
{
    //ProfilePic_URL
    // NSString *strProfilePic_URL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [dicFB valueForKey:@"id"]];
    
    NSMutableDictionary *retVal = [[NSMutableDictionary alloc] init];
    [retVal setValue:[dicFB valueForKey:@"email"] forKey:@"EmailAddress"];
    [retVal setValue:[dicFB valueForKey:@"first_name"] forKey:@"FirstName"];
    [retVal setValue:[dicFB valueForKey:@"last_name"] forKey:@"LastName"];
    [retVal setValue:[dicFB valueForKey:@"id"] forKey:@"FacebookID"];
    [retVal setValue:@"" forKey:@"Profile_PIC"];
    [retVal setValue:@"" forKey:@"DeviceToken"];
    
    return retVal;
}

@end
