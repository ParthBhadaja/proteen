//
//  PT_GoogleSignInClass.h
//  ProTeen
//
//  Created by Apple 1 on 15/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT_ImportClasses.h"

@protocol googleSignInDelegate <NSObject>

-(void)sendDictGoogleSignIn :(GTLPlusPerson *)dict :(NSError *)err;

@end

@interface PT_GoogleSignInClass : NSObject<GPPSignInDelegate>
{
    UIButton *btnIndicatorObj;
    UIViewController *viewControllerObj;
}

-(void)googleSignIn:(UIViewController*)vCtr withIndicatorText:(NSString*)strIndText IndicatorButton:(UIButton *)btnIndicator;

@property (strong,nonatomic)id<googleSignInDelegate>delegateGoogleSignIn;


@end
