//
//  PT_GoogleSignInClass.m
//  ProTeen
//
//  Created by Apple 1 on 15/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_GoogleSignInClass.h"

@implementation PT_GoogleSignInClass
@synthesize delegateGoogleSignIn;

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
    }
    
    return self;
}


-(void)googleSignIn:(UIViewController*)vCtr withIndicatorText:(NSString*)strIndText IndicatorButton:(UIButton *)btnIndicator;
{
    btnIndicatorObj=btnIndicator;
    viewControllerObj=vCtr;
    btnIndicator.useActivityIndicator=YES;
    [btnIndicator activityIndicatorVisibility:YES withViewController:vCtr withTitle:@"" withColor:[UIColor whiteColor]];
    
    [GPPSignIn sharedInstance].clientID = GOOGLE_CLIENT_ID;
    [GPPSignIn sharedInstance].scopes= [NSArray arrayWithObjects:kGTLAuthScopePlusLogin,@"profile", nil];
    [GPPSignIn sharedInstance].shouldFetchGoogleUserID=YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail=YES;
    [GPPSignIn sharedInstance].delegate=self;
    [[GPPSignIn sharedInstance] authenticate];
}

#pragma mark - GPP Sign In Delegate Method

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    if (!error)
    {
        if (error)
        {
            [SVProgressHUD dismiss];
            [delegateGoogleSignIn sendDictGoogleSignIn:nil :error];
        }
        else
        {
            //[self refreshInterfaceBasedOnSignIn];
            
            GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
            
            //            NSLog(@"email %@ ", [NSString stringWithFormat:@"Email: %@",[GPPSignIn sharedInstance].authentication.userEmail]);
            //            NSLog(@"Received error %@ and auth object %@",error, auth);
            
            // 1. Create a |GTLServicePlus| instance to send a request to Google+.
            GTLServicePlus* plusService = [[GTLServicePlus alloc] init] ;
            plusService.retryEnabled = YES;
            
            // 2. Set a valid |GTMOAuth2Authentication| object as the authorizer.
            [plusService setAuthorizer:[GPPSignIn sharedInstance].authentication];
            
            // 3. Use the "v1" version of the Google+ API.*
            plusService.apiVersion = @"v1";
            [plusService executeQuery:query
                    completionHandler:^(GTLServiceTicket *ticket,
                                        GTLPlusPerson *person,
                                        NSError *error) {
                        if (error)
                        {
                            [SVProgressHUD dismiss];
                            [delegateGoogleSignIn sendDictGoogleSignIn:nil :error];
                            //Handle Error
                        }
                        else
                        {
                            [delegateGoogleSignIn sendDictGoogleSignIn:person :nil];

                            NSLog(@"Email= %@", [GPPSignIn sharedInstance].authentication.userEmail);
                            NSLog(@"GoogleID=%@", person.identifier);
                            NSLog(@"User Name=%@", [person.name.givenName stringByAppendingFormat:@" %@", person.name.familyName]);
                            NSLog(@"Gender=%@", person.gender);
                            NSLog(@"%@",[[person valueForKey:@"image"]valueForKey:@"url"]);
                            NSLog(@"%@",person.displayName);
                            
                            [btnIndicatorObj activityIndicatorVisibility:NO withViewController:viewControllerObj withTitle:@"Sign In" withColor:[UIColor whiteColor]];
                           
                        }
                    }];
        }
        
    }
    else
    {
        NSLog(@"%@",error);
    }
}

-(void)refreshInterfaceBasedOnSignIn
{
    if ([[GPPSignIn sharedInstance] authentication])
    {
        // The user is signed in.
        NSLog(@"hi");
        // Perform other actions here, such as showing a sign-out button
    } else {
        
        [[GPPSignIn sharedInstance]authentication];
        //         Perform other actions here
    }
}


@end
