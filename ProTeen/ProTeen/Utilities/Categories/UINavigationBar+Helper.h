//
//  UINavigationBar+Helper.h
//  OpenHouse
//
//  Created by Bhuriyo Patel on 29/01/16.
//  Copyright © 2016 Bhuriyo Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (Helper)
- (void)setBottomBorderColor:(UIColor *)color height:(CGFloat)height;
@end
