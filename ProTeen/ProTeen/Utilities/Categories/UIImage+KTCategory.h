//
//  UIImageAdditions.h

#import <Foundation/Foundation.h>
#import <Accelerate/Accelerate.h>
#import <float.h>
#import <UIKit/UIKit.h>

@interface UIImage (KTCategory)

- (UIImage *)imageScaleAspectToMaxSize:(CGFloat)newSize;
- (UIImage *)imageScaleAndCropToMaxSize:(CGSize)newSize;
- (UIImage *)imageReduceSize:(CGSize)newSize;
@end
