//
//  NSObject+Validation.m
//  MiMedic
//
//  Created by MAC107 on 17/07/14.
//  Copyright (c) 2014 tatva. All rights reserved.
//

#import "NSObject+Validation.h"

#define allTrim_Chat( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ]
#define allTrim( object ) [object stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet] ]

@implementation NSObject (Validation)
/*- (id)RemoveNull
{
    id object = self;
    if ([object isKindOfClass:(id)[NSNull class]])
    {
        return @"";
    }
    else if ([object isKindOfClass:[NSString class]])
    {
        NSString *str = (id)self;
        if(str == nil)
        {
            return @"";
        }
        else if(str == (id)[NSNull null] ||
                [str caseInsensitiveCompare:@"(null)"] == NSOrderedSame ||
                [str caseInsensitiveCompare:@"<null>"] == NSOrderedSame ||
                [str caseInsensitiveCompare:@"null"] == NSOrderedSame ||
                [str caseInsensitiveCompare:@""] == NSOrderedSame ||
                [str length]==0)
        {
            return @"";
        }
        else
        {
            return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        }
    }
    else
        return object;
}*/

- (id)RemoveNullFromString:(BOOL)isString
{
    id object = self;
    if ([object isKindOfClass:[NSNull class]] || object == nil) {
        return (isString == YES) ? @"" : nil;
    } else if ([object isKindOfClass:[NSString class]]) {
        if( object == nil || ([object respondsToSelector:@selector(length)] && [(NSString *)object length] == 0) || [object caseInsensitiveCompare:@"(null)"] == NSOrderedSame || [object caseInsensitiveCompare:@"<null>"] == NSOrderedSame || [object caseInsensitiveCompare:@"null"] == NSOrderedSame || [object caseInsensitiveCompare:@""] == NSOrderedSame) {
            return @"";
        }
        else{
            return allTrim_Chat(object);
        }
    } else if ([object isKindOfClass:[NSArray class]]) {
        if(object == nil || ([object respondsToSelector:@selector(count)] && [(NSArray *)object count] == 0)) {
            return (isString == YES) ? @"" : nil;
        }
    } else if ([object isKindOfClass:[NSDictionary class]]) {
        if(object == nil || ([object respondsToSelector:@selector(count)] && [(NSDictionary *)object count] == 0)) {
            return (isString == YES) ? @"" : nil;
        }
    } else if ([object isKindOfClass:[NSData class]]) {
        if(object == nil || ([object respondsToSelector:@selector(length)] && [(NSData *)object length] == 0)) {
            return (isString == YES) ? @"" : nil;
        }
    } else if ([object isKindOfClass:[UIImage class]]) {
        if(object == nil) {
            return (isString == YES) ? @"" : nil;
        }
    } else if ([object isKindOfClass:[NSDate class]]) {
        if(object == nil || ([object respondsToSelector:@selector(length)] && allTrim(object).length == 0)) {
            return (isString == YES) ? @"" : nil;
        }
    }
    return object;
}

@end
