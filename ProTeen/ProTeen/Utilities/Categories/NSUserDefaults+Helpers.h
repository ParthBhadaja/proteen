//
//  NSUserDefaults+Helpers.h

#import <Foundation/Foundation.h>

@interface NSUserDefaults (Helpers)

/* convenience method to save a given object for a given key */
+ (void)saveObject:(id)object forKey:(NSString *)key;

/* convenience method to return an object for a given key */
+ (id)retrieveObjectForKey:(NSString *)key;

/* convenience method to delete a value for a given key */
+ (void)deleteObjectForKey:(NSString *)key;

@end
