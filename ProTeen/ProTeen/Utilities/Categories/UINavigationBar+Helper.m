//
//  UINavigationBar+Helper.m
//  OpenHouse
//
//  Created by Bhuriyo Patel on 29/01/16.
//  Copyright © 2016 Bhuriyo Patel. All rights reserved.
//

#import "UINavigationBar+Helper.h"

@implementation UINavigationBar (Helper)

- (void)setBottomBorderColor:(UIColor *)color height:(CGFloat)height{
    
    UIView *bottomBorderView = (UIView*)[self viewWithTag:1111];
    
    if (bottomBorderView == nil) {
        CGRect bottomBorderRect = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), height);
        UIView *bottomBorder = [[UIView alloc] initWithFrame:bottomBorderRect];
        bottomBorder.tag = 1111;
        [bottomBorder setBackgroundColor:color];
        [self addSubview:bottomBorder];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            [bottomBorderView setBackgroundColor:color];
        }];
    }

   
}

@end
