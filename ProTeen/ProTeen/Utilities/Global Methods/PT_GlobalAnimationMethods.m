//
//  PT_GlobalAnimationMethods.m
//  ProTeen
//
//  Created by Apple 1 on 14/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_GlobalAnimationMethods.h"
#import "PT_ImportClasses.h"

@implementation PT_GlobalAnimationMethods

+(void)shakingAnimaiton : (UITextField *)textfield
{
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.06];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([textfield center].x - 10.0f, [textfield center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([textfield center].x + 10.0f, [textfield center].y)]];
    [[textfield layer] addAnimation:animation forKey:@"position"];
}

+(void)AlertViewAnimation : (NSString *)strTitle AlertMessage:(NSString *)strMessage
{
    DXAlertView *alert = [[DXAlertView alloc] initWithTitle:strTitle contentText:strMessage leftButtonTitle:nil rightButtonTitle:@"OK"];
    [alert show];
    alert.rightBlock = ^() {
        
    };
    alert.dismissBlock = ^() {
        
    };
}

+(void)AlertViewAnimation : (NSString *)strTitle AlertMessage:(NSString *)strMessege
            RightSideBlock:(void(^)(id response))rightblockCall
             LeftSideBlock:(void(^)(id responseleft))leftblockCall
{
    DXAlertView *alert = [[DXAlertView alloc] initWithTitle:strTitle contentText:strMessege leftButtonTitle:@"YES" rightButtonTitle:@"NO"];
    [alert show];
    
    alert.leftBlock = ^() {
        rightblockCall(@"");
    };
    alert.rightBlock = ^() {
        leftblockCall(@"");
    };
    alert.dismissBlock = ^() {
    };
}

+(CABasicAnimation *)fadeInAndOutAnimation
{
    CABasicAnimation *fadeInAndOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeInAndOut.duration = 1.90;
    fadeInAndOut.autoreverses = NO;
    fadeInAndOut.fromValue = [NSNumber numberWithFloat:0.0];
    fadeInAndOut.toValue = [NSNumber numberWithFloat:1.0];
    fadeInAndOut.repeatCount = 0;
    fadeInAndOut.fillMode = kCAFillModeBoth;
    return fadeInAndOut;
}

@end
