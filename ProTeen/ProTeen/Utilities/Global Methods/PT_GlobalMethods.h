//
//  PT_GlobalMethods.h
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PT_ImportClasses.h"

@protocol imagePickingDelegate <NSObject>

-(void)returnImage :(UIImage *)img;

@end

@protocol datePickerDelegate <NSObject>

-(void)returnDate :(NSString *)strDate;

@end


@interface PT_GlobalMethods : NSObject<UIImagePickerControllerDelegate,UITextFieldDelegate,UINavigationControllerDelegate>
{
    
     /*----- UIImagePickerController -----*/
    
    UIImagePickerController *imgPickerControllerObj;
    UINavigationController *navigationControllerObj;
    
    /*----- UIDatePicker -----*/
    
    UIDatePicker *datePicker;
    
    /*----- UIPickerView -----*/
    
    UIPickerView *pickerViewObj;
    
    /*----- NSString -----*/

    NSString *strDatePickerDate;
    
    /*----- UIActivityIndicatorView -----*/

    UIActivityIndicatorView *indicator;
    
}

@property (strong ,nonatomic) UITextField *dateTextField;
@property(nonatomic)id<imagePickingDelegate>delegate;
@property(nonatomic)id<datePickerDelegate>delegateDate;


/*----- set attributes to navigation controller item -----*/
+ (void)setAttributeswithBarButtonItem:(UIBarButtonItem*)bar_btn withFont:(UIFont*)titlefont withColor:(UIColor*)titleColor;

/*----- create custom button for barbutton item -----*/
+ (UIButton*)createCustomButtonwithActiveImage:(NSString*)img_active withInActiveImage:(NSString*)img_inactive withTarget:(id)target action:(SEL)action;

/*----- alertview for iOS 7 & 8 -----*/
+ (void)showAlertwithTitle:(NSString *)title withMessage:(NSString *)msg withViewController:(UIViewController *)viewCtr;

/*----- pop to specific view  -----*/
+(void)popToSpecificView:(UINavigationController*)nav_name ViewXibName:(NSString*)strXibName WithAnimation:(BOOL)animated;
+ (BOOL)viewExistInNavigationCtr:(UINavigationController*)navCtr ViewCtr:(Class)aClass;

/*----- return attributed text  -----*/
+(NSMutableAttributedString*)retAttributedTextWithString:(NSString*)plainText withAttributedText:(NSString*)attributedText withAttributes:(NSDictionary*)dicAttributes;

/*----- phone number with us style -----*/
+ (NSString*) phoneNumberWithUSStyle:(NSString*)phoneString;

/*----- date convertor -----*/
+ (NSString*)GetDateStringFromNSDate:(NSDate*)Curr_date Formatter:(NSString*)strFormatter;
+ (NSString*)GetDateFromUTCTimeZone:(NSDate*)Curr_date Formatter:(NSString*)strFormatter;

/*----- get top most view controller -----*/
+ (UIViewController*)topViewController;

/*----- find height for text -----*/
+ (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font;

/*----- find width for text -----*/
+ (CGFloat)findWidthForText:(NSString *)text havingHeight:(CGFloat)heightValue andFont:(UIFont *)font;

/*----- disable interactive gesture -----*/
+ (void)disableInteractiveGesture:(UINavigationController*)nav_Ctr;

/*----- returnbar button item -----*/
+ (UIBarButtonItem*)returnUIBarButtonItem:(UIViewController *)viewC withSelector:(SEL)mySelector withImage:(NSString*)strImageName;

/*----- alertview with Gotit -----*/
+ (void)showAlertwithTitle:(NSString *)title withMessage:(NSString *)msg withViewController:(UIViewController *)viewCtr withCompletion:(void (^)(void))completion;

/*----- get time string from seconds -----*/
+ (NSString *)getTimeStringFromSeconds:(double)seconds;

/*----- get video size in bytes/KB/MB/GB/TB -----*/
+ (id)transformedValueBytes:(id)value;

/*----- get unique id -----*/
+ (NSString*)getUniqueID;

/*----- internet connection checl -----*/
+ (BOOL)checkConnection:(void (^)(void))completion;

/*----- Email Validation Check -----*/
+(BOOL)checkEmailValidation :(NSString *)str;

/*----- Mobile Validation Check -----*/
+ (BOOL)validatePhone:(NSString *)phoneNumber;

/*----- Set button Border and BackGround -----*/

+(void)setButtonBorderAndBackground :(UIButton *)btn  CornerRadius:(CGFloat)radiusValue borderWidth:(CGFloat)borderWidth;

+(void)setButtonColor : (UIButton *)button BorderColor:(UIColor *)backColor;

/*----- Set Textfield Empty -----*/

+(void)setTextfieldEmpty :(UIScrollView *)scrollView;

/*------- Password Validation ------*/

+(BOOL)isPasswordValid:(NSString *)pwd;

/*------- Open Alert Controller For Image Piking ------*/

-(void)OpenAlertForImagePicker:(UIImagePickerController *)imgPicker ViewController:(UIViewController *)viewController navigationViewController:(UINavigationController *)navigation;

/*------ Open Activity ViewController --------*/

+(void)OpenActivityViewController :(NSMutableArray *)arr navigationControlelr:(UINavigationController *)navigation;


/*------- Check Space And Emoji On Textfield ----- */

+(BOOL)checkSpaceAndEmojiOnTextfield :(UITextField *)textfield;
+(BOOL)checkWhiteSpaceOnTextfield  :(UITextField *)textfield passrange:(NSRange)range string:(NSString *)str;
/*------ set DatePicker On Textfield -------*/

-(void)setDatePickerOnTextfield : (UITextField *)textfield;

/*------ set UIPicker On Textfield -------*/
-(void)setPickerOnTextfield : (UITextField *)textfield andDelegate:(id)delegates;

/*------ Reload UIPicker -------*/
-(void)ReloadPicker;

/*------- Get County And County Code ------*/

+(NSMutableDictionary *)getCounty;

/*------- Set Button Enabled And Disabled ------*/

+(void)setButtonEnabled:(UIButton *)btn;
+(void)setButtonDisabled:(UIButton *)btn;

@end
