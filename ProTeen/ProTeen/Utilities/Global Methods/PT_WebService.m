//
//  PT_WebService.m
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_WebService.h"
#import "PT_ImportClasses.h"

@implementation PT_WebService

/*----- post Method -----*/
+ (void)callPostWithURLStringwithParameters:(NSMutableDictionary*)dicParameters withViewCtr:(UIViewController*)viewCtr withCompletionHandler:(void (^)(NSURLSessionDataTask *task, id responseObject, NSError * error))completion
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
#if IS_DEBUG
    
    if (dicParameters.count > 0) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dicParameters options:NSJSONWritingPrettyPrinted error:nil];
        
        NSString *someString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"Request : %@\n-----------------------\nRequest : %@",WEB_SERVICE_URL,someString);
    }
    else
    {
    }
    
#endif
    
    [kAFClient POST:WEB_SERVICE_URL parameters:dicParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

#if IS_DEBUG
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:nil];
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSLog(@"Response : %@",jsonString);
        }
#endif
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (completion){
                completion(task, responseObject, nil);
            }
        }
        else{
            if (completion){
                completion(task, nil, nil);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (completion){
            completion(task, nil, error);
        }
    }];
}

/*----- get Method -----*/
+ (void)callGetWithURL:(NSString*)strMethod withViewCtr:(UIViewController*)viewCtr withCompletionHandler:(void (^)(NSURLSessionDataTask *task, id responseObject, NSError * error))completion
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
#if IS_DEBUG
    NSLog(@"Request : %@",strMethod);
#endif
    
    [kAFClient GET:strMethod parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
#if IS_DEBUG
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:nil];
            
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            NSLog(@"Response : %@",jsonString);
        }
#endif
        
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            if (completion){
                completion(task, responseObject, nil);
            }
        }
        else{
            if (completion){
                completion(task, nil, nil);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (completion){
            completion(task, nil, error);
        }
    }];
    
}

@end
