//
//  PT_GlobalAnimationMethods.h
//  ProTeen
//
//  Created by Apple 1 on 14/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PT_GlobalAnimationMethods : NSObject

+(void)shakingAnimaiton : (UITextField *)textfield;

+(void)AlertViewAnimation : (NSString *)strTitle AlertMessage:(NSString *)strMessage;
+(void)AlertViewAnimation : (NSString *)strTitle AlertMessage:(NSString *)strMessege
            RightSideBlock:(void(^)(id response))rightblockCall
            LeftSideBlock:(void(^)(id responseleft))leftblockCall;

+(CABasicAnimation *)fadeInAndOutAnimation;

@end
