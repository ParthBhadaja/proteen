//
//  PT_GlobalMethods.m
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_GlobalMethods.h"
#import "SignUpViewController.h"

@implementation PT_GlobalMethods
@synthesize delegate,delegateDate;

- (id) init
{
    self = [super init];
    
    if (self != nil)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            imgPickerControllerObj = [[UIImagePickerController alloc]init];
            imgPickerControllerObj.delegate=self;
            imgPickerControllerObj.allowsEditing=YES;
            imgPickerControllerObj.navigationBar.translucent= NO;
            imgPickerControllerObj.navigationBar.barTintColor =NAVIGATION_COLOR;
            imgPickerControllerObj.navigationBar.tintColor = [UIColor whiteColor];
            [imgPickerControllerObj.navigationBar setTitleTextAttributes:
             @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
            
            indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        });
        
    }
    
    return self;
}

/*----- set attributes to navigation controller item -----*/
+ (void)setAttributeswithBarButtonItem:(UIBarButtonItem*)bar_btn withFont:(UIFont*)titlefont withColor:(UIColor*)titleColor
{
    [bar_btn setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:titleColor,NSForegroundColorAttributeName,titlefont,NSFontAttributeName, nil] forState:UIControlStateNormal];
}

/*----- create custom button for barbutton item -----*/
+ (UIButton*)createCustomButtonwithActiveImage:(NSString*)img_active withInActiveImage:(NSString*)img_inactive withTarget:(id)target action:(SEL)action
{
    UIImage *imgActive = UIImageNamed(img_active);
    UIImage *imgInActive = UIImageNamed(img_inactive);
    CGRect frameimg = CGRectMake(0, 0, imgActive.size.width, imgActive.size.height);
    UIButton *retVal = [[UIButton alloc] initWithFrame:frameimg];
    [retVal setImage:imgInActive forState:UIControlStateNormal];
    [retVal setImage:imgActive forState:UIControlStateSelected];
    [retVal addTarget:target action:action     forControlEvents:UIControlEventTouchUpInside];
    
    return retVal;
}

/*----- alertview for iOS 8 > -----*/
+ (void)showAlertwithTitle:(NSString *)title withMessage:(NSString *)msg withViewController:(UIViewController *)viewCtr
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel  handler:^(UIAlertAction * action)
                                    {[alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
    
    [alert addAction:defaultAction];
    [viewCtr presentViewController:alert animated:YES completion:nil];
}

/*----- pop to specific view  -----*/
+(void)popToSpecificView:(UINavigationController*)nav_name ViewXibName:(NSString*)strXibName WithAnimation:(BOOL)animated
{
    NSArray *vList = [nav_name viewControllers];
    for (int i= (int)[vList count]-1; i>=0; --i)
    {
        UIViewController *view = [vList objectAtIndex:i];
        if ([view.nibName isEqualToString:strXibName])
        {
            [nav_name popToViewController:view animated:animated];
            break;
        }
    }
}

+ (BOOL)viewExistInNavigationCtr:(UINavigationController*)navCtr ViewCtr:(Class)aClass
{
    BOOL viewExist = NO;
    NSArray *viewControlles = navCtr.viewControllers;
    
    for (UIViewController *viewCtrinNav in viewControlles){
        if ([viewCtrinNav isKindOfClass:aClass]) {
            
            viewExist = YES;
            break;
        }
    }
    
    return viewExist;
}

/*----- return attributed text  -----*/
+(NSMutableAttributedString*)retAttributedTextWithString:(NSString*)plainText withAttributedText:(NSString*)attributedText withAttributes:(NSDictionary*)dicAttributes
{
    NSMutableAttributedString *retVal = [[NSMutableAttributedString alloc] initWithString:plainText];
    
    NSRange textRange = [plainText rangeOfString:attributedText];
    
    [retVal setAttributes:dicAttributes range:textRange];
    
    return retVal;
}

/*----- phone number with us style -----*/
+ (NSString*) phoneNumberWithUSStyle:(NSString*)phoneString
{
    static NSCharacterSet* set = nil;
    if (set == nil){
        set = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    }
    phoneString = [[phoneString componentsSeparatedByCharactersInSet:set] componentsJoinedByString:@""];
    switch (phoneString.length) {
        case 7: return [NSString stringWithFormat:@"%@-%@", [phoneString substringToIndex:3], [phoneString substringFromIndex:3]];
        case 10: return [NSString stringWithFormat:@"(%@) %@-%@", [phoneString substringToIndex:3], [phoneString substringWithRange:NSMakeRange(3, 3)],[phoneString substringFromIndex:6]];
        case 11: return [NSString stringWithFormat:@"%@ (%@) %@-%@", [phoneString substringToIndex:1], [phoneString substringWithRange:NSMakeRange(1, 3)], [phoneString substringWithRange:NSMakeRange(4, 3)], [phoneString substringFromIndex:7]];
        case 12: return [NSString stringWithFormat:@"+%@ (%@) %@-%@", [phoneString substringToIndex:2], [phoneString substringWithRange:NSMakeRange(2, 3)], [phoneString substringWithRange:NSMakeRange(5, 3)], [phoneString substringFromIndex:8]];
        default: return phoneString;
    }
}

/*----- date convertor -----*/
+ (NSString*)GetDateStringFromNSDate:(NSDate*)Curr_date Formatter:(NSString*)strFormatter
{
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSCalendar *sysCalendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:strFormatter];
    dateFormatter.calendar = sysCalendar;
    [dateFormatter setLocale:enUSPOSIXLocale];
    NSString *strStartDate = [dateFormatter stringFromDate:Curr_date];
    return strStartDate;
}
+ (NSString*)GetDateFromUTCTimeZone:(NSDate*)Curr_date Formatter:(NSString*)strFormatter
{
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    NSCalendar *sysCalendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateFormatter *dateFormatter= [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [dateFormatter setDateFormat:strFormatter];
    dateFormatter.calendar = sysCalendar;
    [dateFormatter setLocale:enUSPOSIXLocale];
    
    NSString *strStartDate = [dateFormatter stringFromDate:Curr_date];
    return strStartDate;
}

/*----- get top most view controller -----*/
+ (UIViewController*)topViewController {
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}

/*----- find height for text -----*/
+ (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize;
    if (text) {
        CGSize size;
        
        CGRect frame = [text boundingRectWithSize:CGSizeMake(widthValue, CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        size = CGSizeMake(frame.size.width, frame.size.height+1);
        result = MAX(size.height, result); //At least one row
    }
    return result;
}

/*----- find width for text -----*/
+ (CGFloat)findWidthForText:(NSString *)text havingHeight:(CGFloat)heightValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize;
    if (text) {
        CGSize size;
        
        CGRect frame = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, heightValue)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        size = CGSizeMake(frame.size.width+1, frame.size.height);
        result = MAX(size.width, result); //At least one row
    }
    return result;
}

/*----- disable interactive gesture -----*/
+ (void)disableInteractiveGesture:(UINavigationController*)nav_Ctr{
    nav_Ctr.interactivePopGestureRecognizer.enabled = NO;
    nav_Ctr.interactivePopGestureRecognizer.delegate = nil;
}

/*----- returnbar button item -----*/
+ (UIBarButtonItem*)returnUIBarButtonItem:(UIViewController *)viewC withSelector:(SEL)mySelector withImage:(NSString*)strImageName
{
    UIImage *buttonImage = UIImageNamed(strImageName);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:viewC action:mySelector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *retVal = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    return retVal;
}

/*----- alertview with Gotit -----*/
+ (void)showAlertwithTitle:(NSString *)title withMessage:(NSString *)msg withViewController:(UIViewController *)viewCtr withCompletion:(void (^)(void))completion{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Got it!" style:UIAlertActionStyleCancel  handler:^(UIAlertAction * action)
                                    {
                                        if (completion) {
                                            completion();
                                        }
                                    }];
    [alert addAction:defaultAction];
    
    [viewCtr presentViewController:alert animated:YES completion:nil];
}

/*----- get time string from seconds -----*/
+ (NSString *)getTimeStringFromSeconds:(double)seconds
{
    NSDateComponentsFormatter *dcFormatter = [[NSDateComponentsFormatter alloc] init];
    dcFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
    dcFormatter.allowedUnits = NSCalendarUnitMinute | NSCalendarUnitSecond;
    dcFormatter.unitsStyle = NSDateComponentsFormatterUnitsStylePositional;
    return [dcFormatter stringFromTimeInterval:seconds];
}

/*----- get video size in bytes/KB/MB/GB/TB -----*/
+ (id)transformedValueBytes:(id)value
{
    return [NSByteCountFormatter stringFromByteCount:[value doubleValue] countStyle:NSByteCountFormatterCountStyleFile];
    
    /*double convertedValue = [value doubleValue];
     int multiplyFactor = 0;
     
     NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"Kb",@"Mb",@"Gb",@"Tb",nil];
     
     while (convertedValue > 1024) {
     convertedValue /= 1024;
     multiplyFactor++;
     }
     
     return [NSString stringWithFormat:@"%4.2f %@",convertedValue, [tokens objectAtIndex:multiplyFactor]];*/
}

/*----- get unique id -----*/
+ (NSString*)getUniqueID
{
    NSUUID  *UUID = [NSUUID UUID];
    NSString* uniqueID = [UUID UUIDString];
    return uniqueID;
}

/*----- internet connection checl -----*/
+ (BOOL)checkConnection:(void (^)(void))completion
{
    const char *host_name = "www.google.com";
    BOOL _isDataSourceAvailable = NO;
    Boolean success;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL,host_name);
    SCNetworkReachabilityFlags flags;
    success = SCNetworkReachabilityGetFlags(reachability, &flags);
    _isDataSourceAvailable = success &&
    (flags & kSCNetworkFlagsReachable) &&
    !(flags & kSCNetworkFlagsConnectionRequired);
    
    CFRelease(reachability);
    
    if (completion) {
        completion();
    }
    return _isDataSourceAvailable;
}

/*----- Email Validation Check -----*/

+(BOOL)checkEmailValidation :(NSString *)str
{
    BOOL stricterFilter = NO; 
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:str];
}

/*----- Mobile Validation Check -----*/

+ (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

/*----- Set button Border and BackGround -----*/

+(void)setButtonBorderAndBackground :(UIButton *)btn  CornerRadius:(CGFloat)radiusValue borderWidth:(CGFloat)borderWidth
{
    btn.layer.cornerRadius=radiusValue;
    btn.layer.borderWidth=borderWidth;
}

+(void)setButtonColor : (UIButton *)button BorderColor:(UIColor *)backColor
{
    button.layer.borderColor=backColor.CGColor;
    button.layer.borderWidth=1;
}

/*----- Set Textfield Empty -----*/


+(void)setTextfieldEmpty :(UIScrollView *)scrollView
{
    for (UITextField *txt in scrollView.subviews)
    {
        if ([txt isKindOfClass:[UITextField class]])
        {
            txt.text=@"";
        }
    }
}

/*------- Password Validation ------*/

+(BOOL)isPasswordValid:(NSString *)pwd
{
    if (pwd.length >= 6 && pwd.length < maximumPasswordLength)
    {
        return YES;
    }
    return NO;
    
    /*
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    if ( [pwd length]<6 || [pwd length]>20 )
        return NO;  // too long or too short
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        return NO;  // no letter
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
    rang = [pwd rangeOfCharacterFromSet:upperCaseChars];
    if ( !rang.length )
        return NO;  // no uppercase letter;
    rang = [pwd rangeOfCharacterFromSet:lowerCaseChars];
    if ( !rang.length )
        return NO;  // no lowerCase Chars;
    return YES;
     
     */
}


#pragma mark - ImagePickerController Process

/*------- Open Alert Controller For Image Piking ------*/


-(void)OpenAlertForImagePicker:(UIImagePickerController *)imgPicker ViewController:(UIViewController *)viewController navigationViewController:(UINavigationController *)navigation
{
    
    navigationControllerObj = navigation;
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Select" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"CameraRoll", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        imgPickerControllerObj.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [navigation presentViewController:imgPickerControllerObj animated:YES completion:nil];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"TakePhoto", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    
                                    if ([UIImagePickerController  isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront] || [UIImagePickerController  isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront])
                                    {
                                        imgPickerControllerObj.sourceType = UIImagePickerControllerSourceTypeCamera;
                                        [navigation presentViewController:imgPickerControllerObj animated:YES completion:nil];
                                    }
                                    else
                                    {
                                        /*------ Display Custom Alert Message -----*/
                                        [PT_GlobalAnimationMethods AlertViewAnimation:APP_NAME AlertMessage:@"Camera Device not Available"];
                                    }
                                }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action)
                                {
                                }]];
    
    [navigation presentViewController:alertController animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [delegate returnImage:[info objectForKey:UIImagePickerControllerEditedImage]];
//    imgViewObj.image = [info objectForKey:UIImagePickerControllerEditedImage];
    [navigationControllerObj dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [navigationControllerObj dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ///////////////////////////////

/*------ Open Activity ViewController --------*/

+(void)OpenActivityViewController :(NSMutableArray *)arr navigationControlelr:(UINavigationController *)navigation
{
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:arr applicationActivities:nil];
    
    [navigation presentViewController:activityController animated:YES completion:nil];
}

/*------- Check Space And Emoji On Textfield ----- */


+(BOOL)checkSpaceAndEmojiOnTextfield :(UITextField *)textfield
{
    if ([[[textfield textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textfield textInputMode] primaryLanguage])
    {
        return NO;
    }
    
    return YES;
}

+(BOOL)checkWhiteSpaceOnTextfield  :(UITextField *)textfield passrange:(NSRange)range string:(NSString *)str
{
    NSString *resultingString = [textfield.text stringByReplacingCharactersInRange:range withString:str];
    
    NSCharacterSet *whitespaceSet = [NSCharacterSet whitespaceCharacterSet];
    if  ([resultingString rangeOfCharacterFromSet:whitespaceSet].location == NSNotFound)
    {
        return YES;
    }
    
    return NO;
}

/*-------- Set DatePicker on Textfield ------*/

-(void)setDatePickerOnTextfield : (UITextField *)textfield
{
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-13];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-17];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:minDate];
    [datePicker addTarget:self action:@selector(dateChanged) forControlEvents:UIControlEventValueChanged];
    datePicker.backgroundColor = [UIColor clearColor];
    [datePicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SYSTEM_SCREEN_SIZE.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    toolBar.translucent = NO;
    toolBar.barTintColor = TEXTFIELD_BORDER_COLOR;
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneClicked)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor whiteColor];
    
    datePicker.frame = CGRectMake(0, toolBar.frame.size.height, SYSTEM_SCREEN_SIZE.width, 200);
    
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SYSTEM_SCREEN_SIZE.width, toolBar.frame.size.height + datePicker.frame.size.height)];
    inputView.backgroundColor = NAVIGATION_COLOR;
    [inputView addSubview:datePicker];
    [inputView addSubview:toolBar];
    textfield.inputView= inputView;
    _dateTextField = textfield ;
}

-(void)dateChanged
{
    strDatePickerDate = [PT_GlobalMethods GetDateStringFromNSDate:datePicker.date Formatter:@"MM/dd/yyyy"];
    [delegateDate returnDate:strDatePickerDate];
}

-(void)doneClicked
{
    [_dateTextField resignFirstResponder];
}



- (void)setPickerOnTextfield : (UITextField *)textfield andDelegate:(id)delegates
{
    /*------ Picker View Allocation -------*/
    pickerViewObj = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 44, SYSTEM_SCREEN_SIZE.width, 200)];
    [pickerViewObj setDataSource: delegates];
    [pickerViewObj setDelegate: delegates];
    pickerViewObj.showsSelectionIndicator = YES;
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, SYSTEM_SCREEN_SIZE.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    toolBar.translucent = NO;
    toolBar.barTintColor = TEXTFIELD_BORDER_COLOR;
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:delegates
                                                                     action:@selector(doneClicked)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor whiteColor];
    
    
    
    
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SYSTEM_SCREEN_SIZE.width, toolBar.frame.size.height + pickerViewObj.frame.size.height)];
    inputView.backgroundColor = NAVIGATION_COLOR;
    [inputView addSubview:pickerViewObj];
    [inputView addSubview:toolBar];
    textfield.inputView= inputView;
}

-(void)ReloadPicker{
    [pickerViewObj reloadAllComponents];
}


#pragma mark - /////////////////////// 

/*------- Get County And County Code ------*/

+(NSMutableDictionary *)getCounty
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString *country = [usLocale displayNameForKey: NSLocaleCountryCode value: countryCode];

    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setObject:countryCode forKey:kCOUNTRY_CODE];
    [dict setObject:country forKey:kCOUNTRY];

    return dict;
}

/*------- Set Button Enabled And Disabled ------*/

+(void)setButtonEnabled:(UIButton *)btn
{
    [btn setEnabled:TRUE];
    [btn setTitleColor:RGBACOLOR(255.0f, 255.0f, 255.0f, 1.0f) forState:UIControlStateNormal];
    
}

+(void)setButtonDisabled:(UIButton *)btn
{
    [btn setEnabled:FALSE];
    [btn setTitleColor:RGBACOLOR(255.0f, 255.0f, 255.0f, 0.50f) forState:UIControlStateNormal];

}

@end
