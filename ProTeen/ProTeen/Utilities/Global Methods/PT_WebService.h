//
//  PT_WebService.h
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PT_WebService : NSObject

/*----- post Method -----*/
+ (void)callPostWithURLStringwithParameters:(NSMutableDictionary*)dicParameters withViewCtr:(UIViewController*)viewCtr withCompletionHandler:(void (^)(NSURLSessionDataTask *task, id responseObject, NSError * error))completion;

/*----- get Method -----*/
+ (void)callGetWithURL:(NSString*)strMethod withViewCtr:(UIViewController*)viewCtr withCompletionHandler:(void (^)(NSURLSessionDataTask *task, id responseObject, NSError * error))completion;

@end
