//
//  PT_Constants.h
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#define MAIN_URL @""
#define IS_DEBUG 1
#define APP_NAME @"ProTeen"
#define APP_STORE_ID @""
#define APP_URL @""

#define WebSite_URL @""

#define SYSTEM_SCREEN_SIZE [[UIScreen mainScreen] bounds].size

#define IPHONE4_HEIGHT 480
#define IPHONE5_WIDTH  320
#define IPHONE5_HEIGHT 568
#define kDEV_PROPROTIONAL_Height(val) ([UIScreen mainScreen].bounds.size.height / IPHONE5_HEIGHT * val)
#define kDEV_PROPROTIONAL_Width(val) ([UIScreen mainScreen].bounds.size.width / IPHONE5_WIDTH * val)

#define IPHONE6_PLUS_WIDTH  414[txtPincode setLeftModeView:@"PincodeIcon" width:14.0 Height:17.0];

#define IPHONE6_PLUS_HEIGHT 736
#define kDEV_PROPROTIONAL_FONTS(val) ([UIScreen mainScreen].bounds.size.width / IPHONE6_PLUS_WIDTH * val)

#define CGRECT_MAKE(x,y,w,h) CGRectMake(x,y,w,h)

#define appDelegate (AppDelegate *) [[UIApplication sharedApplication] delegate];


/*----- RGB Color -----*/
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0f]
#define RGBCGCOLOR(r,g,b) [[UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0f] CGColor]
#define RGBACOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:a]
#define LABLE_BG_COLOR  RGBACOLOR(245,134,52,1.0)
#define TEXTFIELD_BORDER_COLOR  RGBACOLOR(230,106,69,0.62)
#define NAVIGATION_COLOR RGBACOLOR(245, 134, 52, 0.62)

/*----- Font -----*/
#define FONT(fontname,fontsize)             [UIFont fontWithName:fontname size:fontsize]
#define OpenSans_ExtraboldItalic(fontsize)  [UIFont fontWithName:@"OpenSans-ExtraboldItalic" size:fontsize]
#define OpenSans_SemiboldItalic(fontsize)   [UIFont fontWithName:@"OpenSans-SemiboldItalic"  size:fontsize]
#define OpenSans_Extrabold(fontsize)        [UIFont fontWithName:@"OpenSans-Extrabold"       size:fontsize]
#define OpenSans_BoldItalic(fontsize)       [UIFont fontWithName:@"OpenSans-BoldItalic"      size:fontsize]
#define OpenSans_Italic(fontsize)           [UIFont fontWithName:@"OpenSans-Italic"          size:fontsize]
#define OpenSans_Semibold(fontsize)         [UIFont fontWithName:@"OpenSans-Semibold"        size:fontsize]
#define OpenSans_Light(fontsize)            [UIFont fontWithName:@"OpenSans-Light"           size:fontsize]
#define OpenSans(fontsize)                  [UIFont fontWithName:@"OpenSans"                 size:fontsize]
#define OpenSansLight_Italic(fontsize)      [UIFont fontWithName:@"OpenSansLight-Italic"     size:fontsize]
#define OpenSans_Bold(fontsize)             [UIFont fontWithName:@"OpenSans-Bold"            size:fontsize]

/*----- Webservice Default Paramater -----*/
#define kTimeOutInterval 300
#define kURLGet @"GET"
#define kURLPost @"POST"

#define NSStringFromInteger(value) [NSString stringWithFormat:@"%ld",(long)value]
#define NSStringFromFloat(value)   [NSString stringWithFormat:@"%f",value]
#define UIImageNamed(imageName)   [UIImage imageNamed:imageName]
#define URLString(str) [NSURL URLWithString:str]

/*----- Webservice Methods -----*/

#define kAFClient [AFAPIClient sharedClient]


/*----- Check iOS Version -----*/
#define iOS_VERSION_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define iOS_VERSION_GREATER_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define iOS_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define iOS_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define iOS_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


/*----- NSUserDefaults -----*/
#define kUser_Detail @"PT_UserDetail"

/*----- constant value -----*/
#define minimumPasswordLength 8
#define maximumPasswordLength 20

#define maxPhoneNoLength 10

#define IndexPath(section, row) [NSIndexPath indexPathForRow:row inSection:section]

#define Not_CorrectFormate NSLocalizedString(@"strResponseInvalid", nil)

/*----- social sharing keys/secrets ------*/
/*----- google plus ------*/

/*----- facebook ------*/
#define FACEBOOK_APPID @"493861650798559"

#define GOOGLE_CLIENT_ID @"419639159037-kbvsd1rb4d34hchj5a253papdan52o6n.apps.googleusercontent.com"

