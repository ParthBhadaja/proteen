//
//  PT_ControlConstants.h
//  ProTeen
//
//  Created by Apple 1 on 23/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#define WEB_SERVICE_URL @"http://proteen.inexture.com/webservice"

#pragma mark - /////////////////////// WEB SERVICE PARAMETER ////////////////////


#define kCOUNTRY @"country"
#define kEMAIL @"email"
#define kPASSWORD @"password"
#define kNAME @"name"
#define kBIRTH_DATE @"birth_date"
#define kDEVICE_TOKEN @"devicetoken"
#define kMOBILE @"mobile"
#define kCOUNTRY_CODE @"country_code"
#define kPROFILE_PIC @"profile_pic"
#define kSOLCIAL_ID @"social_id"
#define kDEVICE_TYPE @"devicetype"
#define kBODY @"body"

#pragma mark - /////////////////////// Navigation Bar Proporties ////////////////////

/*------  Navigation Bar Proporties -------*/

#define NAVIGATION_BACK_BUTTON UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 23, 40)];img.image=[UIImage imageNamed:@"BackIcon"];UIBarButtonItem *leftBarButton = [[UIBarButtonItem alloc]initWithImage:img.image style:UIBarButtonItemStylePlain target:self action:@selector(btnBack)];self.navigationItem.leftBarButtonItem=leftBarButton;

/*----- Push and Pop Navigation  -----*/

#define POP_VIEW_CONTROLLER [self.navigationController popViewControllerAnimated:YES];

#define PUSH_VIEW_CONTROLLER(viewcontroller,viewcontrollername) viewcontroller *viewControllerObj = [[viewcontroller alloc]initWithNibName:[NSString stringWithFormat:@"%@",viewcontrollername] bundle:nil]; [self.navigationController pushViewController:viewControllerObj animated:YES];
