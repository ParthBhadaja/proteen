//
//  PT_TextfieldClass.h
//  ProTeen
//
//  Created by Apple 1 on 15/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PT_ImportClasses.h"



IB_DESIGNABLE

@interface PT_TextfieldClass : UITextField
@property (strong, nonatomic) IBInspectable UIImage *imageLeftmode;

@end
