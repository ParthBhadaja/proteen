//
//  PT_TextfieldClass.m
//  ProTeen
//
//  Created by Apple 1 on 15/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "PT_TextfieldClass.h"

#define leftViewWidth 30

@implementation PT_TextfieldClass


- (void)awakeFromNib
{
    [self setupUI];
}

- (void)setupUI
{
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setValue:RGBCOLOR(255, 255, 255) forKeyPath:@"_placeholderLabel.textColor"];
    [self setFont:[UIFont fontWithName:@"OpenSans-Light" size:11.0]];
    self.layer.borderColor = TEXTFIELD_BORDER_COLOR.CGColor;
    self.layer.borderWidth = 1;
    self.textColor = RGBCOLOR(255, 255, 255);
    self.tintColor = [UIColor whiteColor];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, leftViewWidth, self.frame.size.height)];
    [self setLeftViewMode:UITextFieldViewModeAlways];
    [self setLeftView:paddingView];

    UIImageView *imgView = [[UIImageView alloc] initWithFrame:paddingView.frame];
    imgView.image = self.imageLeftmode;
    imgView.contentMode = UIViewContentModeCenter;
    [paddingView addSubview:imgView];
    
    if (self.clearButtonMode == UITextFieldViewModeWhileEditing)
    {
        UIButton *clearButton = [self valueForKey:@"_clearButton"];
        [clearButton setImage:[UIImage imageNamed:@"ClearButton"] forState:UIControlStateNormal];
        [clearButton setImage:[UIImage imageNamed:@"ClearButton"] forState:UIControlStateHighlighted];
    }
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
