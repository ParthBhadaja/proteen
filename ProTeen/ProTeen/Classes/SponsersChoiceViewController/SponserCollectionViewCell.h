//
//  SponserCollectionViewCell.h
//  ProTeen
//
//  Created by Apple 1 on 16/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SponserCollectionViewCell : UICollectionViewCell

@property (strong,nonatomic)IBOutlet UIImageView *imgSponserObj;

@end
