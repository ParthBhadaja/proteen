//
//  SponsersChoiceViewController.m
//  ProTeen
//
//  Created by Apple 1 on 16/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "SponsersChoiceViewController.h"
#import "PT_ImportClasses.h"
#import "SponserCollectionViewCell.h"

@interface SponsersChoiceViewController ()
{
    IBOutlet UICollectionView *collectionViewObj;
}

-(IBAction)btnDone:(id)sender;

@end

@implementation SponsersChoiceViewController
@synthesize delegateRemoveSponserView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [collectionViewObj registerNib:[UINib nibWithNibName:@"SponserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SponserCell"];
    
    
    [PT_WebService callPostWithURLStringwithParameters:[self returnParameterDict] withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
     {
         if (error != nil)
         {
             NSLog(@"%@",responseObject);
         }
         else
         {
             
        }
     }];
    
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear:animated];
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
    
}

#pragma mark - Create Parameter Dictionary

-(NSMutableDictionary *)returnParameterDict
{
    NSMutableDictionary *subdict = [NSMutableDictionary new];
    NSMutableDictionary *maindict = [NSMutableDictionary new];
    [maindict setObject:@"getSponsors" forKey:kNAME];
    [maindict setObject:subdict forKey:kBODY];
    return maindict;
}

#pragma mark - CollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 16;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SponserCollectionViewCell *cell = [collectionViewObj dequeueReusableCellWithReuseIdentifier:@"SponserCell" forIndexPath:indexPath];
//    cell.imgSponserObj.image=[UIImage imageNamed:@"MobileIcon"];
//    cell.imgSponserObj.backgroundColor=[UIColor whiteColor];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SponserCollectionViewCell *cell =(SponserCollectionViewCell*)[collectionViewObj cellForItemAtIndexPath:indexPath];
    [UIView animateWithDuration:.30 animations:^{
        cell.imgSponserObj.layer.borderColor=[UIColor redColor].CGColor;
        cell.imgSponserObj.layer.borderWidth=6;
        cell.transform=CGAffineTransformMakeScale(1.1, 1.1);
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger width =  collectionViewObj.frame.size.width/3;
    NSInteger height = collectionViewObj.frame.size.height/2;
    return CGSizeMake((CGFloat)width-8,(CGFloat)height-18);
}

#pragma mark - Done Button Action

-(IBAction)btnDone:(id)sender
{
    [delegateRemoveSponserView removeSponserView];
}

#pragma mark - Back And Forward Button Action
/*Common Method for backWard and forWard button*/
-(IBAction)btnBackForwardClick:(UIButton*)sender
{
    if (sender.tag==1) //Means Backward Button Press
    {
        if (backOrforw==2)
        {
            backOrforw=1;
            [collectionViewObj scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:6 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
        else if (backOrforw==1)
        {
            backOrforw=0;
            [collectionViewObj scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
    }
    else // Means Forward Button Press
    {
        if (backOrforw==0)
        {
            backOrforw=1;
            [collectionViewObj scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:11 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
        else if(backOrforw==1)
        {
            backOrforw=2;
            [collectionViewObj scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:15 inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
