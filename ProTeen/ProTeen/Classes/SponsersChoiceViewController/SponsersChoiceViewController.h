//
//  SponsersChoiceViewController.h
//  ProTeen
//
//  Created by Apple 1 on 16/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//



#import <UIKit/UIKit.h>


@protocol delegateRomeveSponser <NSObject>



-(void)removeSponserView;

@end

@interface SponsersChoiceViewController : UIViewController
{
    int backOrforw;
}
@property(nonatomic)id<delegateRomeveSponser>delegateRemoveSponserView;
@property(weak,nonatomic)IBOutlet UIButton *btnBackward;
@property(weak,nonatomic)IBOutlet UIButton *btnForward;
-(IBAction)btnBackForwardClick:(UIButton*)sender;

@end
