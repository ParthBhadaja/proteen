//
//  SignUpViewController.m
//  ProTeen
//
//  Created by Apple 1 on 09/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "SignUpViewController.h"
#import "ProfileViewController.h"
#import "SponsersChoiceViewController.h"
#import "selfSponsorViewController.h"
#import "PT_GlobalMethods.h"
#import "PT_ImportClasses.h"


@interface SignUpViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,imagePickingDelegate,datePickerDelegate,googleSignInDelegate,delegateRomeveSponser>
{
    /*----- Textfield -----*/
    IBOutlet PT_TextfieldClass *txtName;
    IBOutlet PT_TextfieldClass *txtNickName;
    IBOutlet PT_TextfieldClass *txtEmail;
    IBOutlet PT_TextfieldClass *txtPincode;
    IBOutlet PT_TextfieldClass *txtBirthDate;
    IBOutlet PT_TextfieldClass *txtPhoneNumber;
    IBOutlet PT_TextfieldClass *txtCountry;
    IBOutlet PT_TextfieldClass *txtPassword;

    /*----- UIButton -----*/
    IBOutlet UIButton *btnSignUp;
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btnUploadImg;
    IBOutlet UIButton *btnFacebook;
    IBOutlet UIButton *btnGoogle;
    
    
    /*----- UIScrollView -----*/
    IBOutlet TPKeyboardAvoidingScrollView *scrollViewObj;
  
    /*----- UIView -----*/
    IBOutlet UIView *viewMaleFemaleObj;
    
    /*----- UIImageView -----*/
    UIImageView *imgViewObj;
    
    /*----- UIAlertController -----*/
    UIAlertController *alertController;
    
    /*----- UIImagePickerController -----*/
    UIImagePickerController *imgPickerControllerObj;

    /*----- UIDatePicker -----*/
    UIDatePicker *datePicker;
    
    /*----- PT_GlobalMethods -------*/
    PT_GlobalMethods *globalMethodClassObj;
    
    /*------ Sponsers View Controller ------*/
    SponsersChoiceViewController *sponsersChoiceViewControllerObj;
    
    /*------ Self Sponsor View Controller ------*/
    selfSponsorViewController *selfSponsorViewControllerObj;
    
    NSString *strMaleFemale;
    
    /*------ UILable ----------*/
    IBOutlet UILabel *lblTagOr;
    IBOutlet UILabel *lblTagMale;
    IBOutlet UILabel *lblTagFemale;
    IBOutlet UILabel *lblTagMySelf;
    IBOutlet UILabel *lblTagMyChoice;
    IBOutlet UILabel *lblTagNone;
    IBOutlet UILabel *lblDisplaymsg;
    
    /*------ UILable ----------*/
    NSMutableArray *arrCountryList;
    UIActivityIndicatorView *indicator;
    IBOutlet NSLayoutConstraint *viewHeight;
    
}
@end

@implementation SignUpViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [PT_GlobalMethods getCounty];
    viewHeight.constant = [UIScreen mainScreen].bounds.size.height;
   
    /*----- Set Button Properties -----*/
    btnUploadImg.layer.borderWidth = 1;
    [PT_GlobalMethods setButtonColor:btnUploadImg BorderColor:TEXTFIELD_BORDER_COLOR];
    [PT_GlobalMethods setButtonDisabled:btnSignUp];
    
    /*-------- set Country Via Location --------*/
    txtCountry.text = [[PT_GlobalMethods getCounty] objectForKey:kCOUNTRY];
    txtCountry.tintColor=[UIColor clearColor];
    
    /*------ gloabal Method Class Allocation -------*/
    globalMethodClassObj = [[PT_GlobalMethods alloc]init];
    globalMethodClassObj.delegateDate=self;
    [globalMethodClassObj setDatePickerOnTextfield:txtBirthDate];
    [globalMethodClassObj setPickerOnTextfield:txtCountry andDelegate:self];
    
    /*------- Others -------*/
    lblTagOr.clipsToBounds=YES;
    lblTagOr.layer.cornerRadius=lblTagOr.frame.size.width/2;
    strMaleFemale = @"";
    [lblDisplaymsg sizeToFit];
    
    [self setBorder:lblTagMale];
    [self setBorder:lblTagFemale];
    [self setBorder:lblTagMyChoice];
    [self setBorder:lblTagMySelf];
    [self setBorder:lblTagNone];
    
    viewMaleFemaleObj.layer.borderColor=TEXTFIELD_BORDER_COLOR.CGColor;
    viewMaleFemaleObj.layer.borderWidth=1;
    
    /*------ Self Sponsers View Controller ------*/
    selfSponsorViewControllerObj = [[selfSponsorViewController alloc]initWithNibName:@"selfSponsorViewController" bundle:nil];
    //    selfSponsorViewControllerObj.delegateRemoveSponserView=self;
    selfSponsorViewControllerObj.view.alpha=0.0;

    /*------ set Right Mode On Textfield ------*/
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5,(txtCountry.frame.size.height/2)-3,9,6)];
    imgView.image = [UIImage imageNamed:@"DropBox"];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(5,0,20,txtCountry.frame.size.height)];
    /*------ Label for RightBorder ------*/
//    UILabel *lblright = [[UILabel alloc]initWithFrame:CGRectMake(0,1,1,paddingView.frame.size.height-2)];
//    lblright.backgroundColor = TEXTFIELD_BORDER_COLOR;
//    [paddingView addSubview:lblright];
    [paddingView addSubview:imgView];
    [txtCountry setRightViewMode:UITextFieldViewModeAlways];
    [txtCountry setRightView:paddingView];
    
    /*------ Set Scrollview -------*/
  
    scrollViewObj.contentSize=CGSizeMake(0, btnGoogle.frame.size.height+btnGoogle.frame.origin.y);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NAVIGATION_BACK_BUTTON
    self.navigationItem.title = NSLocalizedString(@"titleSignUP", nil);
}

#pragma mark - Textfield Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == txtCountry)
    {
        if (arrCountryList.count == 0)
        {
            indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            indicator.center = txtCountry.inputView.center;
            indicator.color=[UIColor whiteColor];
            [txtCountry.inputView addSubview:indicator];
            [indicator startAnimating];
            
            NSDictionary *dict = [NSDictionary dictionaryWithObject:@"getCountryList" forKey:@"name"];
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                [PT_WebService callPostWithURLStringwithParameters:(NSMutableDictionary *)dict withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error) {
                    
                    [indicator stopAnimating];
                    
                    if ([[responseObject objectForKey:@"status"]boolValue] == FALSE)
                    {
                    }
                    else
                    {
                        arrCountryList = [responseObject objectForKey:@"data"];
                        [globalMethodClassObj ReloadPicker];
                    }
                }];
                
            });
        }
        else
        {
            [globalMethodClassObj ReloadPicker];
        }
        
        
       
    }
    
    return TRUE;
}

-(void)CheckTextFieldsEmpty
{
    /*----- Check Textfield Empty Or not -----*/
    
    if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
        [[txtName.text RemoveNullFromString:YES] length] > 0 &&
        [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
        [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
        [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
        [txtPassword.text length] > 0 &&
        [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
        [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
        [[txtNickName.text RemoveNullFromString:YES] length] > 0)
    {
        [PT_GlobalMethods setButtonEnabled:btnSignUp];
    }
    else
    {
        [PT_GlobalMethods setButtonDisabled:btnSignUp];
    }
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [PT_GlobalMethods setButtonDisabled:btnSignUp];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == txtBirthDate || textField == txtCountry)
    {
        return NO;
    }
    else if (![PT_GlobalMethods checkSpaceAndEmojiOnTextfield:textField])
    {
        return NO;
    }
    
    
    NSString *strText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (textField == txtName)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtNickName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
    }
    if (textField == txtPassword)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtNickName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [string  length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
    }
    else if (textField == txtNickName)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
    }
    else if(textField == txtBirthDate)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtNickName.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
        
    }
    else if (textField == txtEmail)
    {
        if ([[txtNickName.text RemoveNullFromString:YES] length] > 0  &&
            [[txtName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
        
    }
    else if (textField == txtPhoneNumber)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtNickName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtPincode.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
        
    }
    else if (textField == txtPincode)
    {
        if ([[txtEmail.text RemoveNullFromString:YES] length] > 0  &&
            [[txtName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtPhoneNumber.text RemoveNullFromString:YES] length] > 0 &&
            [[txtBirthDate.text RemoveNullFromString:YES] length] > 0 &&
            [[strMaleFemale RemoveNullFromString:YES] length] > 0 &&
            [txtPassword.text length] > 0 &&
            [[txtNickName.text RemoveNullFromString:YES] length] > 0 &&
            [[txtCountry.text RemoveNullFromString:YES] length] > 0 &&
            [[strText RemoveNullFromString:YES] length] > 0)
        {
            [PT_GlobalMethods setButtonEnabled:btnSignUp];
        }
        else
        {
            [PT_GlobalMethods setButtonDisabled:btnSignUp];
        }
        
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtName)
    {
        [txtName resignFirstResponder];
        [txtNickName becomeFirstResponder];
    }
    else if (textField == txtNickName)
    {
        [txtNickName resignFirstResponder];
        [txtBirthDate becomeFirstResponder];
        
    }
    else if(textField == txtBirthDate)
    {
        [textField resignFirstResponder];
        [txtEmail becomeFirstResponder];
    }
    else if (textField == txtEmail)
    {
        [txtEmail resignFirstResponder];
        [txtPassword becomeFirstResponder];
    }
    else if(textField == txtPassword)
    {
        [textField resignFirstResponder];
        [txtPhoneNumber becomeFirstResponder];
    }
    else if (textField == txtPhoneNumber)
    {
        [txtPhoneNumber resignFirstResponder];
        [txtPincode becomeFirstResponder];
    }
    else if (textField == txtPincode)
    {
        [txtPincode resignFirstResponder];
        scrollViewObj.contentOffset = CGPointMake(0, 0);
    }
    return YES;
}

#pragma mark - PickerView Delegate & DataSource Method

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrCountryList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[arrCountryList objectAtIndex:row]objectForKey:@"c_name"];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title =[[arrCountryList objectAtIndex:row]objectForKey:@"c_name"];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    txtCountry.text=[[arrCountryList objectAtIndex:row]objectForKey:@"c_name"];
}

-(void)doneClicked
{
    [txtCountry resignFirstResponder];
}

#pragma mark - All Actions

-(IBAction)btnSignUp:(id)sender
{
    
    if (![PT_GlobalMethods checkEmailValidation:txtEmail.text])
    {
        lblDisplaymsg.text =  NSLocalizedString(@"strInvalidEmail", nil);
    }
    else if (![PT_GlobalMethods validatePhone:txtPhoneNumber.text])
    {
      lblDisplaymsg.text = NSLocalizedString(@"strInvalidMobile", nil);
    }
    else
    {
        
        /*-------- Call Webservice ----------*/
        [PT_WebService callPostWithURLStringwithParameters:[self returnParameterDict] withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
         {
             
             if (error != nil)
             {
                 lblDisplaymsg.text = error.localizedDescription;
             }
             else if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]])
             {
                 [PT_GlobalMethods setTextfieldEmpty:scrollViewObj];
                 
                 ProfileViewController *profileviewControllerObj = [[ProfileViewController alloc]initWithNibName:@"ProfileViewController" bundle:nil];
                 profileviewControllerObj.imProfileObj=imgViewObj.image;
                 PUSH_VIEW_CONTROLLER(ProfileViewController, @"ProfileViewController")
             }
             else
             {
                 //response data is not in valid formate
                 lblDisplaymsg.text = Not_CorrectFormate;
             }
            
         }];
    }
}

-(IBAction)btnMaleFemale:(UIButton *)sender
{
    if (sender == btnMale)
    {
        strMaleFemale=@"M";
        [self setBackGround:lblTagMale unselectLable:lblTagFemale];
        lblTagMale.layer.borderWidth=0;
        lblTagFemale.layer.borderWidth=1;
    }
    else
    {
        strMaleFemale=@"F";
        [self setBackGround:lblTagFemale unselectLable:lblTagMale];
        lblTagMale.layer.borderWidth=1;
        lblTagFemale.layer.borderWidth=0;
    }
    
    [self CheckTextFieldsEmpty];
}

-(IBAction)btnUploadPhoto:(id)sender
{
    [self.view endEditing:YES];
    globalMethodClassObj.delegate = self;
    [globalMethodClassObj OpenAlertForImagePicker:imgPickerControllerObj ViewController:[PT_GlobalMethods topViewController] navigationViewController:self.navigationController];
}

-(IBAction)btnFacebook:(id)sender
{
    /*------ Facebook Class ------*/
    
    PT_FacebookClass *facebookClassObj = [[PT_FacebookClass alloc]init];
    [facebookClassObj loginWithViewCtr:[PT_GlobalMethods topViewController] indicatorButton:btnFacebook withIndicatorText:@"" withCompletionHandler:^(NSDictionary *Dic)
     {
         NSLog(@"Facebook Dictionary :  %@",Dic);
     }];

}

-(IBAction)btnGoogle:(id)sender
{
    /*------ Google Class ------*/
    PT_GoogleSignInClass *googleClassObj = [[PT_GoogleSignInClass alloc]init];
    googleClassObj.delegateGoogleSignIn = self;
    [googleClassObj googleSignIn:[PT_GlobalMethods topViewController] withIndicatorText:@"Loading" IndicatorButton:btnGoogle];
}

-(IBAction)selectSponser:(UIButton *)sender
{
    // 1 My Self -- 2 My Choice -- 3 None

    [self clearBackGroundColor];
    
    if (sender.tag == 1)
    {
        lblTagMySelf.layer.borderWidth=0;
        lblTagMySelf.backgroundColor=TEXTFIELD_BORDER_COLOR;
        
        [UIView animateWithDuration:0.3 animations:^{
            selfSponsorViewControllerObj.view.alpha=1.0;
            UIWindow *currentWindow = [[UIApplication sharedApplication] keyWindow];
            
            [currentWindow addSubview:selfSponsorViewControllerObj.view];
        } completion:^(BOOL finished) {
        }];
    }
    else if (sender.tag == 2)
    {
        
        /*------ Sponsers View Controller ------*/
        
        sponsersChoiceViewControllerObj = [[SponsersChoiceViewController alloc]initWithNibName:@"SponsersChoiceViewController" bundle:nil];
        sponsersChoiceViewControllerObj.delegateRemoveSponserView=self;
        sponsersChoiceViewControllerObj.view.alpha=0.0;
        
        lblTagMyChoice.layer.borderWidth=0;
        lblTagMyChoice.backgroundColor=TEXTFIELD_BORDER_COLOR;
        self.navigationController.navigationBar.userInteractionEnabled=FALSE;
        
        [UIView animateWithDuration:0.3 animations:^{
            sponsersChoiceViewControllerObj.view.alpha=1.0;
            UIWindow *currentWindow = [[UIApplication sharedApplication] keyWindow];
            
            [currentWindow addSubview:sponsersChoiceViewControllerObj.view];
        } completion:^(BOOL finished) {
        }];
    }
    else
    {
        lblTagNone.layer.borderWidth=0;
        lblTagNone.backgroundColor=TEXTFIELD_BORDER_COLOR;
    }
}

#pragma mark - Get DatePicker Value Via Gloabal Method

-(void)returnDate :(NSString *)strDate
{
    txtBirthDate.text = strDate;
}

#pragma mark - Get Image Via Gloabal Method ( Delegate )

-(void)returnImage :(UIImage *)img
{
    [btnUploadImg setTitle:@"" forState:UIControlStateNormal];
    [btnUploadImg setBackgroundImage:img forState:UIControlStateNormal];
//    imgViewObj.image=img;
}

#pragma mark - Get GoogleSign In Response ( Delegate )

-(void)sendDictGoogleSignIn :(GTLPlusPerson *)dict :(NSError *)err
{
    if (dict != nil)
    {
    }
}

#pragma mark - Remove SponsersView

-(void)removeSponserView
{
    self.navigationController.navigationBar.userInteractionEnabled=TRUE;
    [UIView animateWithDuration:0.3 animations:^{
        sponsersChoiceViewControllerObj.view.alpha=0.0;
    } completion:^(BOOL finished) {
        [sponsersChoiceViewControllerObj.view removeFromSuperview];
    }];
}


#pragma mark - Check Male/Female

-(void)checkMaleFemale : (UIButton *)sender
{
   [self.view endEditing:YES];
    
    if (sender.tag == 1)
    {
        strMaleFemale=@"M";
        [btnMale setBackgroundColor:TEXTFIELD_BORDER_COLOR];
        [btnFemale setBackgroundColor:[UIColor clearColor]];
    }
    else
    {
        strMaleFemale=@"F";
        [btnFemale setBackgroundColor:TEXTFIELD_BORDER_COLOR];
        [btnMale setBackgroundColor:[UIColor clearColor]];
    }
}

#pragma mark - set Lable Border

-(void)setBorder : (UILabel *)lbl
{
    lbl.layer.borderColor=TEXTFIELD_BORDER_COLOR.CGColor;
    lbl.layer.borderWidth=1;
}

#pragma mark - set Border and Backgound of Male/Female

-(void)setBackGround :(UILabel *)lblBoder unselectLable:(UILabel *)lblBG
{
    lblBG.backgroundColor=[UIColor clearColor];
    lblBoder.backgroundColor=TEXTFIELD_BORDER_COLOR;
}

#pragma mark - Other Methods

-(void)btnBack
{
    [self.view endEditing:YES];
//    [self.navigationController popViewControllerAnimated:YES];
    POP_VIEW_CONTROLLER
}

-(void)clearBackGroundColor
{
    [lblTagMySelf setBackgroundColor:[UIColor clearColor]];
    [lblTagMyChoice setBackgroundColor:[UIColor clearColor]];
    [lblTagNone setBackgroundColor:[UIColor clearColor]];
    lblTagMyChoice.layer.borderWidth=1;
    lblTagMySelf.layer.borderWidth=1;
    lblTagNone.layer.borderWidth=1;
}

#pragma mark - Return Paramter Dictionary

-(NSMutableDictionary *)returnParameterDict
{
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [mainDict setObject:@"signup" forKey:kNAME];
    
    [dict setObject:[NSString stringWithFormat:@"%@ %@",txtName.text,txtNickName.text] forKey:kNAME];
    [dict setObject:txtBirthDate.text forKey:kBIRTH_DATE];
    [dict setObject:txtEmail.text forKey:kEMAIL];
    [dict setObject:txtPassword.text forKey:kPASSWORD];
    [dict setObject:txtPhoneNumber.text forKey:kMOBILE];
    [dict setObject:[[PT_GlobalMethods getCounty] objectForKey:kCOUNTRY] forKey:kCOUNTRY_CODE];
    [dict setObject:[[PT_GlobalMethods getCounty] objectForKey:kCOUNTRY_CODE] forKey:kCOUNTRY];
    [dict setObject:@"" forKey:kPROFILE_PIC];
    [dict setObject:@"" forKey:kSOLCIAL_ID];
    [dict setObject:kAppDel.device_Token forKey:kDEVICE_TOKEN];
    [dict setObject:@"1" forKey:kDEVICE_TYPE];
    
    [mainDict setObject:dict forKey:kBODY];

    return mainDict;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
