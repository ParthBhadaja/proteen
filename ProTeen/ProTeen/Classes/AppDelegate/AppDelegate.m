//
//  AppDelegate.m
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <GooglePlus/GooglePlus.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 
    /*---- Check User Auto Login ------*/
    if ([self checkAutoLogin])
    {
    }
    else
    {
    }
   
    /*------ Google Sign In Authentication ----*/
    [[GPPSignIn sharedInstance]authentication];
    
    /*----- register push notification -----*/
    [self registerForRemoteNotification];
    
    /*------ Set Status Bar Font Light Content -------*/
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    /*----- window initialization -----*/
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    /*----- Set RootViewController -------*/
    SplashViewController *splaceScreenViewObj = [[SplashViewController alloc]initWithNibName:@"SplashViewController" bundle:nil];
    self.window.rootViewController = [self setNavigationWithViewCtr:splaceScreenViewObj];
    [self.window makeKeyAndVisible];
    
    // Override point for customization after application launch.
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[GPPSignIn sharedInstance] signOut];
    [[GPPSignIn sharedInstance] disconnect];
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Google 

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication  annotation: (id)annotation
{
    return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
}

#pragma mark - Set Navigation View Controller

- (UINavigationController *)setNavigationWithViewCtr:(SplashViewController*)splaceScreenViewObj
{
    self.navigationCtrWindow = [[UINavigationController alloc]initWithRootViewController:splaceScreenViewObj];
    self.navigationCtrWindow.navigationBar.barTintColor = RGBACOLOR(230,106,69,1.0);
    self.navigationCtrWindow.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationCtrWindow.navigationBarHidden = YES;
    self.navigationCtrWindow.navigationBar.translucent = NO;
    [self.navigationCtrWindow.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor],
       NSFontAttributeName:[UIFont fontWithName:@"OpenSans-Bold" size:16]}];
    return self.navigationCtrWindow;
}


#pragma mark - Check Auto Login ( Model Class nil or not )

-(BOOL)checkAutoLogin
{
    NSData *encodedObject = [NSUserDefaults retrieveObjectForKey:@"response"];
    self.modelClassObj = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    if (self.modelClassObj == nil)
    {
        return NO;
    }
    
    return YES;
}

#pragma mark - For Remote Notification
- (void)registerForRemoteNotification
{
    /*----- let the device know we want to receive push notifications -----*/
    //For iOS 8
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)] && [UIApplication instancesRespondToSelector:@selector(registerForRemoteNotifications)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    //For iOS 7 & less
    /*else if ([UIApplication instancesRespondToSelector:@selector(registerForRemoteNotificationTypes:)])
     {
     UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
     [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
     }*/
}


#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    self.device_Token = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    self.device_Token = @"simulator";
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //NSLog(@"didReceiveRemoteNotification : %@",userInfo);
}

@end
