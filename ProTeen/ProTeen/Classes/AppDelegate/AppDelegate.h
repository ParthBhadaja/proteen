//
//  AppDelegate.h
//  ProTeen
//
//  Created by Bhuriyo Patel on 01/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PT_ImportClasses.h"
#import "SplashViewController.h"

@class ModelClass;

/*----- Appdelegate -----*/
#define kAppDel ((AppDelegate*)[[UIApplication sharedApplication] delegate])

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationCtrWindow;

@property (strong, nonatomic) NSString *device_Token;

@property (nonatomic) ModelClass *modelClassObj;

@end

