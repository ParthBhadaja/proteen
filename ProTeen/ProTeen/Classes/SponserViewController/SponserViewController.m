//
//  SponserViewController.m
//  ProTeen
//
//  Created by Apple 1 on 11/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "SponserViewController.h"
#import "PT_GlobalMethods.h"
#import "PT_ImportClasses.h"


@interface SponserViewController ()
{
    IBOutlet UIButton *btnChooseSponser;
    IBOutlet UIButton *btnEditClubMember;
    IBOutlet UIButton *btnSelfSponser;
    
    
}

#pragma mark - All Actions

-(IBAction)btnMenu:(id)sender;
-(IBAction)btnChooseSponser:(id)sender;
-(IBAction)btnSelfsponser:(id)sender;
-(IBAction)btnEliteClubMember:(id)sender;
-(IBAction)btnBack:(id)sender;
-(IBAction)btnLogout:(id)sender;

@end

@implementation SponserViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - All Actions


/*----- Menu Action ----*/

-(IBAction)btnMenu:(id)sender
{
    
}

/*----- Choose Sponser Action ----*/

-(IBAction)btnChooseSponser:(id)sender
{
}

/*----- Self Sponser Action ----*/


-(IBAction)btnSelfsponser:(UIButton *)sender
{
    [self checkSelectUnselect:sender];
}

/*----- Elite Club Member Action ----*/


-(IBAction)btnEliteClubMember:(UIButton *)sender
{
    [self checkSelectUnselect:sender];
}

/*----- Back Action ----*/


-(IBAction)btnBack:(id)sender
{
    POP_VIEW_CONTROLLER
}

-(IBAction)btnLogout:(id)sender
{
    [PT_GlobalAnimationMethods AlertViewAnimation:@"Alert" AlertMessage:@"Are You Sure You Want to Logout ?" RightSideBlock:^(id response) {
        
        POP_VIEW_CONTROLLER
        
    }LeftSideBlock:^(id responseleft)
     {
         NSLog(@"left");
     }];
}

#pragma mark - Other Methods

/*------- Button Select Unselect check Method ------*/

-(void)checkSelectUnselect : (UIButton *)sender
{
    if (sender.isSelected == YES)
    {
        sender.selected=NO;
    }
    else
    {
        sender.selected=YES;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
