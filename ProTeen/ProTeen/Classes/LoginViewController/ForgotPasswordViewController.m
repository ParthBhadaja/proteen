//
//  ForgotPasswordViewController.m
//  ProTeen
//
//  Created by Apple 1 on 11/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "PT_ImportClasses.h"

@interface ForgotPasswordViewController ()
{
    
    /*----- UITextfield --------*/
    
    IBOutlet PT_TextfieldClass *txtEmail;
    
    /*----- UIScrollview --------*/
    
    IBOutlet TPKeyboardAvoidingScrollView *scrollViewObj;
    
    /*----- UIButton ------*/

    IBOutlet UIButton *btnSend;
    
    IBOutlet UILabel *lblDisplaymsg;

}


-(IBAction)btnSend:(id)sender;

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [lblDisplaymsg sizeToFit];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    NAVIGATION_BACK_BUTTON
    self.navigationItem.title = NSLocalizedString(@"titleForgetPass", nil);
    [PT_GlobalMethods setButtonDisabled:btnSend];
}

#pragma mark - Submit Button Click

-(IBAction)btnSend:(id)sender
{
    
    if (![PT_GlobalMethods checkEmailValidation:txtEmail.text] && ![PT_GlobalMethods validatePhone:txtEmail.text])
    {
        lblDisplaymsg.text = NSLocalizedString(@"strInvalidEmail", nil);
    }
    else
    {

        /*-------- Call Webservice ----------*/
        
        [PT_WebService callPostWithURLStringwithParameters:[self returnParameterDict] withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
            {
                
                if (error != nil)
                {
                    lblDisplaymsg.text = error.localizedDescription;
                }
                else if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]])
                {
                    
                }
                else
                {
                          //response data is not in valid formate
                    lblDisplaymsg.text = Not_CorrectFormate;
                }
        }];
    }
}

#pragma mark - Textfield DelegateMethod

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (![PT_GlobalMethods checkSpaceAndEmojiOnTextfield:textField])
    {
        return NO;
    }
    
    NSString *strText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([[strText RemoveNullFromString:YES] length] > 0)
    {
        [PT_GlobalMethods setButtonEnabled:btnSend];
    }
    else
    {
        [PT_GlobalMethods setButtonDisabled:btnSend];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self btnSend:nil];
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    txtEmail.text = @"";
    [PT_GlobalMethods setButtonDisabled:btnSend];
    return YES;
}

#pragma mark - Other Methods

-(void)btnBack
{
    POP_VIEW_CONTROLLER
}

#pragma mark - //////////////////

-(NSMutableDictionary *)returnParameterDict
{
    NSMutableDictionary *subdict = [NSMutableDictionary new];
    NSMutableDictionary *maindict = [NSMutableDictionary new];
    [maindict setObject:@"forgotPassword" forKey:kNAME];
    [maindict setObject:subdict forKey:kBODY];
    [subdict setObject:txtEmail.text forKey:kEMAIL];
    
    return maindict;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
