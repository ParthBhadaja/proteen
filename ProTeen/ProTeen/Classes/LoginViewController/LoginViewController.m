//
//  LoginViewController.m
//  ProTeen
//
//  Created by Apple 1 on 09/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "ProfileViewController.h"
#import "SponserViewController.h"
#import "ForgotPasswordViewController.h"
#import "PT_ImportClasses.h"
#import "PT_GlobalMethods.h"

@interface LoginViewController ()<UITextFieldDelegate,googleSignInDelegate>
{
    /*----- Textfield -----*/
    IBOutlet PT_TextfieldClass *txtPassword;
    IBOutlet PT_TextfieldClass *txtEmail;
    
    /*----- UIButton -----*/
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnSignUp;
    IBOutlet UIButton *btnGoogle;
    IBOutlet UIButton *btnFacebookObj;
    
    /*----- TPKeyboardAvoidingScrollView -----*/
    IBOutlet TPKeyboardAvoidingScrollView *scrollViewObj;
    
    /*------ Facebook Class ------*/
    PT_FacebookClass *facebookClassObj;
    
    /*------ Google Class ------*/
    PT_GoogleSignInClass *googleClassObj;
    
    /*-------- Other -----*/
    IBOutlet UILabel *lblOrTag1;
    IBOutlet UILabel *lblOrTag2;
    IBOutlet UILabel *lblDisplaymsg;
    IBOutlet UIImageView *imgViewLogo;
}

@end

@implementation LoginViewController

#pragma mark - View Did Load


- (void)viewDidLoad
{
    [super viewDidLoad];

    /*----- Set Button Properties -----*/
    [PT_GlobalMethods setButtonDisabled:btnLogin];
    btnLogin.useActivityIndicator = YES;
 
    /*-------- Other ------*/
    [self cornerRadius:lblOrTag1];
    [self cornerRadius:lblOrTag2];
    
    [lblDisplaymsg sizeToFit];
    
    scrollViewObj.contentSize = CGSizeMake(0, btnGoogle.frame.size.height+btnGoogle.frame.origin.y);
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    lblDisplaymsg.text = @"";
    [self ZoomInZoomOut];
}


#pragma mark - Textfield Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    lblDisplaymsg.text = @"";
    
    NSString *strText = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (textField == txtEmail)
    {
        [self checkRequieredFiledwithEmail:strText withPassword:txtPassword.text];
    }
    else if (textField == txtPassword && [string isEqualToString:@""])
    {
        [PT_GlobalMethods setButtonDisabled:btnLogin];
    }
    else if (textField == txtPassword)
    {
        [self checkRequieredFiledwithEmail:txtEmail.text withPassword:strText];
    }

    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txtEmail)
    {
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword)
    {
        [self btnLogin:nil];
    }
    
    return YES;
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    lblDisplaymsg.text = @"";
    [PT_GlobalMethods setButtonDisabled:btnLogin];
    return YES;
}

- (void)checkRequieredFiledwithEmail:(NSString*)strEmail withPassword:(NSString*)strPassword
{
    if ([[strEmail RemoveNullFromString:YES] length] > 0 && [strPassword length] > 0)
    {
        [PT_GlobalMethods setButtonEnabled:btnLogin];
    }
    else
    {
        [PT_GlobalMethods setButtonDisabled:btnLogin];
    }
}

#pragma mark - All Actions

/*------ Login -------*/
-(IBAction)btnLogin:(id)sender
{
    [self.view endEditing:YES];
    
    if (![PT_GlobalMethods checkEmailValidation:txtEmail.text] && ![PT_GlobalMethods validatePhone:txtEmail.text])
    {
        lblDisplaymsg.text = NSLocalizedString(@"strInvalidEmail", nil);
    }
    else if (![PT_GlobalMethods isPasswordValid:txtPassword.text])
    {
        lblDisplaymsg.text = NSLocalizedString(@"strPasswordError", nil);
    }
    else
    {
        
        [btnLogin activityIndicatorVisibility:YES withViewController:[PT_GlobalMethods topViewController] withTitle:@"" withColor:[UIColor whiteColor]];
        
        /*-------- Call Webservice ----------*/
        
        [PT_WebService callPostWithURLStringwithParameters:[self returnParameterDict] withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
         {
             if (error != nil)
             {
                 lblDisplaymsg.text = error.localizedDescription;
             }
             else if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]])
             {
                [btnLogin activityIndicatorVisibility:NO withViewController:[PT_GlobalMethods topViewController] withTitle:@"Login" withColor:[UIColor whiteColor]];
                 
                 if ([[responseObject objectForKey:@"status"]boolValue] == FALSE)
                 {
                     lblDisplaymsg.text = [responseObject objectForKey:@"message"];
                 }
                 else
                 {

                     /*------- Save Response Data in Model Class ------*/
                     kAppDel.modelClassObj = [[ModelClass alloc]initwithDictionary:responseObject];
                     [kAppDel.modelClassObj saveCustomObject:kAppDel.modelClassObj key:@"response"];
                     
                     
                     /*------ Call Update Device Token Web Service ------*/
                      
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       
                         [PT_WebService callPostWithURLStringwithParameters:[self returnDeviceTokenDict] withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
                          {
                              if (error != nil)
                              {
                              }
                              else if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]])
                              {
                                  if ([[responseObject objectForKey:@"status"]boolValue] == FALSE)
                                  {
                                  }
                                  else
                                  {
                                  }
                              }
                              else
                              {
                                  //response data is not in valid formate
                                  lblDisplaymsg.text = Not_CorrectFormate;
                              }
                          }];
                         
                     });
                     
                     
                     /*------ Display Custom Alert Message -----*/
                     [PT_GlobalAnimationMethods AlertViewAnimation:@"Congratulations" AlertMessage:@"login successfully"];
                     [PT_GlobalMethods setTextfieldEmpty:scrollViewObj];
                     
                     /*----- Push After Login -------*/
//                     PUSH_VIEW_CONTROLLER(ProfileViewController, @"ProfileViewController");
                 }
             }
             else
             {
                 //response data is not in valid formate
                 lblDisplaymsg.text = Not_CorrectFormate;
             }
         }];
    }
}

/*------ Sign Up -------*/

-(IBAction)btnSignUp:(id)sender
{
    [self.view endEditing:YES];
    
    /*
    SignUpViewController *signUpViewControllerObj = [[SignUpViewController alloc]initWithNibName:@"SignUpViewController" bundle:nil];
    [self.navigationController pushViewController:signUpViewControllerObj animated:YES];
     */
    PUSH_VIEW_CONTROLLER(SignUpViewController, @"SignUpViewController")
    
}

/*------ Facebook -------*/

-(IBAction)btnFacebook:(id)sender
{
    /*--------  Facebook Class Allocation ------*/
    facebookClassObj = [[PT_FacebookClass alloc]init];
    
    [facebookClassObj loginWithViewCtr:[PT_GlobalMethods topViewController] indicatorButton:btnFacebookObj withIndicatorText:@"" withCompletionHandler:^(NSDictionary *Dic)
    {
        NSLog(@"Facebook Dictionary :  %@",Dic);
        dispatch_async(dispatch_get_main_queue(), ^(){
         
        });
    }];
}

/*------ Google -------*/

-(IBAction)btnGoogle:(id)sender
{
    /*--------  google Class Allocation ------*/
    googleClassObj = [[PT_GoogleSignInClass alloc]init];
    
    googleClassObj.delegateGoogleSignIn=self;
    [googleClassObj googleSignIn:[PT_GlobalMethods topViewController] withIndicatorText:@"" IndicatorButton:btnGoogle];
}

/*------ Forgot -------*/

-(IBAction)btnForgot:(id)sender
{
    [self.view endEditing:YES];
    PUSH_VIEW_CONTROLLER(ForgotPasswordViewController, @"ForgotPasswordViewController");
}

#pragma mark - Google Plus Sign in Delegate Call

-(void)sendDictGoogleSignIn :(GTLPlusPerson *)dict :(NSError *)err
{
    if (dict != nil)
    {
    }
    else
    {
    }
}

#pragma mark - Create Corner Radius

-(void)cornerRadius : (UILabel *)lbl
{
    lbl.clipsToBounds = YES;
    lbl.layer.cornerRadius = lbl.frame.size.width/2;
}

-(void)ZoomInZoomOut
{
    [btnFacebookObj.layer addAnimation:[PT_GlobalAnimationMethods fadeInAndOutAnimation] forKey:nil];
    [btnGoogle.layer addAnimation:[PT_GlobalAnimationMethods fadeInAndOutAnimation] forKey:nil];
}

#pragma mark - Return Login Parameter Dictionary

-(NSMutableDictionary *)returnParameterDict
{
    NSMutableDictionary *subdict = [NSMutableDictionary new];
    NSMutableDictionary *maindict = [NSMutableDictionary new];
    [maindict setObject:@"login" forKey:kNAME];
    [maindict setObject:subdict forKey:kBODY];
    [subdict setObject:txtEmail.text forKey:kEMAIL];
    [subdict setObject:txtPassword.text forKey:kPASSWORD];
    
    return maindict;
}

#pragma mark - Return Update Device Token Parameter Dictionary

-(NSMutableDictionary *)returnDeviceTokenDict
{
    NSMutableDictionary *subdict = [NSMutableDictionary new];
    NSMutableDictionary *maindict = [NSMutableDictionary new];
    [maindict setObject:@"updateDeviceToken" forKey:kNAME];
    [maindict setObject:subdict forKey:kBODY];
    [subdict setObject:kAppDel.modelClassObj.ID forKey:@"userid"];
    [subdict setObject:kAppDel.device_Token forKey:@"devicetoken"];
    [subdict setObject:@"1" forKey:@"devicetype"];
    
    return maindict;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
