//
//  ModelClass.m
//  ProTeen
//
//  Created by Apple 1 on 28/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "ModelClass.h"

@implementation ModelClass

- (id)initwithDictionary : (id)dict
{
    if (self == [super init])
    {
        
        /*------- Seve Response Data ------*/
        
        NSMutableDictionary *dictResponse = [dict objectForKey:@"data"];
        
        self.c_code = [dictResponse objectForKey:@"c_code"];
        self.c_name = [dictResponse objectForKey:@"c_name"];
        self.country_id = [dictResponse objectForKey:@"country_id"];
        self.ID = [dictResponse objectForKey:@"id"];
        self.password = [dictResponse objectForKey:@"password"];
        self.remember_token = [dictResponse objectForKey:@"remember_token"];
        self.sp_admin_name = [dictResponse objectForKey:@"sp_admin_name"];
        self.sp_bank_accountno = [dictResponse objectForKey:@"sp_bank_accountno"];
        self.sp_bank_name = [dictResponse objectForKey:@"sp_bank_name"];
        self.sp_company_name = [dictResponse objectForKey:@"sp_company_name"];
        self.sp_credit = [dictResponse objectForKey:@"sp_credit"];
        self.sp_email = [dictResponse objectForKey:@"sp_email"];
        self.sp_isapproved = [dictResponse objectForKey:@"sp_isapproved"];
        self.sp_logo = [dictResponse objectForKey:@"sp_logo"];
        self.sponser_id = [dictResponse objectForKey:@"sponser_id"];
        self.t_birthdate = [dictResponse objectForKey:@"t_birthdate"];
        self.t_boosterpoints = [dictResponse objectForKey:@"t_boosterpoints"];
        self.t_country = [dictResponse objectForKey:@"t_country"];
        self.t_credit = [dictResponse objectForKey:@"t_credit"];
        self.t_device_token = [dictResponse objectForKey:@"t_device_token"];
        self.t_device_type = [dictResponse objectForKey:@"t_device_type"];
        self.t_email = [dictResponse objectForKey:@"t_email"];
        self.t_gender = [dictResponse objectForKey:@"t_gender"];
        self.t_isfirstlogin = [dictResponse objectForKey:@"t_isfirstlogin"];
        self.t_isverified = [dictResponse objectForKey:@"t_isverified"];
        self.t_level = [dictResponse objectForKey:@"t_level"];
        self.t_location = [dictResponse objectForKey:@"t_location"];
        self.t_name = [dictResponse objectForKey:@"t_name"];
        self.t_nickname = [dictResponse objectForKey:@"t_nickname"];
        self.t_phone = [dictResponse objectForKey:@"t_phone"];
        self.t_photo = [dictResponse objectForKey:@"t_photo"];
        self.t_pincode = [dictResponse objectForKey:@"t_pincode"];
        self.t_school = [dictResponse objectForKey:@"t_school"];
        self.t_social_identifier = [dictResponse objectForKey:@"t_social_identifier"];
        self.t_social_provider = [dictResponse objectForKey:@"t_social_provider"];
        self.t_sponsor_choice = [dictResponse objectForKey:@"t_sponsor_choice"];
        self.t_uniqueid = [dictResponse objectForKey:@"t_uniqueid"];
        self.teenager_id = [dictResponse objectForKey:@"teenager_id"];
        
    }
    return self;
}

/*----- Encode to Decode ( Get Data in Model Class ) -----*/

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init])
    {
        self.sp_bank_name = [decoder decodeObjectForKey:@"sp_bank_name"];
        self.sp_credit = [decoder decodeObjectForKey:@"sp_credit"];
        self.t_pincode = [decoder decodeObjectForKey:@"sp_bank_name"];
        self.ID = [decoder decodeObjectForKey:@"id"];
        self.t_device_type = [decoder decodeObjectForKey:@"t_device_type"];
        self.t_country = [decoder decodeObjectForKey:@"t_country"];
        self.t_gender = [decoder decodeObjectForKey:@"t_gender"];
        self.sp_bank_accountno = [decoder decodeObjectForKey:@"sp_bank_name"];
        self.t_school = [decoder decodeObjectForKey:@"t_school"];
        self.c_name = [decoder decodeObjectForKey:@"c_name"];
        self.sp_email = [decoder decodeObjectForKey:@"sp_email"];
        self.country_id = [decoder decodeObjectForKey:@"country_id"];
        self.t_social_identifier = [decoder decodeObjectForKey:@"t_social_identifier"];
        self.t_birthdate = [decoder decodeObjectForKey:@"t_birthdate"];
        self.t_email = [decoder decodeObjectForKey:@"t_email"];
        self.t_location = [decoder decodeObjectForKey:@"t_location"];
        self.c_code = [decoder decodeObjectForKey:@"c_code"];
        self.t_level = [decoder decodeObjectForKey:@"t_level"];
        self.t_uniqueid = [decoder decodeObjectForKey:@"t_uniqueid"];
        self.t_sponsor_choice = [decoder decodeObjectForKey:@"t_sponsor_choice"];
        self.sp_isapproved = [decoder decodeObjectForKey:@"sp_isapproved"];
        self.sponser_id = [decoder decodeObjectForKey:@"sponser_id"];
        self.t_isverified = [decoder decodeObjectForKey:@"t_isverified"];
        self.teenager_id = [decoder decodeObjectForKey:@"teenager_id"];
        self.remember_token = [decoder decodeObjectForKey:@"remember_token"];
        self.t_social_provider = [decoder decodeObjectForKey:@"t_social_provider"];
        self.t_photo = [decoder decodeObjectForKey:@"t_photo"];
        self.t_credit = [decoder decodeObjectForKey:@"t_credit"];
        self.sp_company_name = [decoder decodeObjectForKey:@"sp_company_name"];
        self.sp_admin_name = [decoder decodeObjectForKey:@"sp_admin_name"];
        self.t_phone = [decoder decodeObjectForKey:@"t_phone"];
        self.t_isfirstlogin = [decoder decodeObjectForKey:@"t_isfirstlogin"];
        self.t_device_token = [decoder decodeObjectForKey:@"t_device_token"];
        self.password = [decoder decodeObjectForKey:@"password"];
        self.t_nickname = [decoder decodeObjectForKey:@"t_nickname"];
        self.t_name = [decoder decodeObjectForKey:@"t_name"];
        self.t_boosterpoints = [decoder decodeObjectForKey:@"t_boosterpoints"];
        self.sp_logo = [decoder decodeObjectForKey:@"sp_logo"];
        
    }
    return self;
}

/*------ Encode All Properties ------*/

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.sp_bank_name forKey:@"sp_bank_name"];
    [coder encodeObject:self.sp_credit forKey:@"sp_credit"];
    [coder encodeObject:self.t_pincode forKey:@"t_pincode"];
    [coder encodeObject:self.ID forKey:@"id"];
    [coder encodeObject:self.t_device_type forKey:@"t_device_type"];
    [coder encodeObject:self.t_country forKey:@"t_country"];
    [coder encodeObject:self.t_gender forKey:@"t_gender"];
    [coder encodeObject:self.sp_bank_accountno forKey:@"sp_bank_accountno"];
    [coder encodeObject:self.t_school forKey:@"t_school"];
    [coder encodeObject:self.c_name forKey:@"c_name"];
    [coder encodeObject:self.sp_email forKey:@"sp_email"];
    [coder encodeObject:self.country_id forKey:@"country_id"];
    [coder encodeObject:self.t_social_identifier forKey:@"t_social_identifier"];
    [coder encodeObject:self.t_birthdate forKey:@"t_birthdate"];
    [coder encodeObject:self.t_email forKey:@"t_email"];
    [coder encodeObject:self.t_location forKey:@"t_location"];
    [coder encodeObject:self.c_code forKey:@"c_code"];
    [coder encodeObject:self.t_level forKey:@"t_level"];
    [coder encodeObject:self.t_uniqueid forKey:@"t_uniqueid"];
    [coder encodeObject:self.t_sponsor_choice forKey:@"t_sponsor_choice"];
    [coder encodeObject:self.sp_isapproved forKey:@"sp_isapproved"];
    [coder encodeObject:self.sponser_id forKey:@"sponser_id"];
    [coder encodeObject:self.t_isverified forKey:@"t_isverified"];
    [coder encodeObject:self.teenager_id forKey:@"teenager_id"];
    [coder encodeObject:self.remember_token forKey:@"remember_token"];
    [coder encodeObject:self.t_social_provider forKey:@"t_social_provider"];
    [coder encodeObject:self.t_photo forKey:@"t_photo"];
    [coder encodeObject:self.t_credit forKey:@"t_credit"];
    [coder encodeObject:self.sp_company_name forKey:@"sp_company_name"];
    [coder encodeObject:self.sp_admin_name forKey:@"sp_admin_name"];
    [coder encodeObject:self.t_phone forKey:@"t_phone"];
    [coder encodeObject:self.t_isfirstlogin forKey:@"t_isfirstlogin"];
    [coder encodeObject:self.t_device_token forKey:@"t_device_token"];
    [coder encodeObject:self.password forKey:@"password"];
    [coder encodeObject:self.t_nickname forKey:@"t_nickname"];
    [coder encodeObject:self.t_name forKey:@"t_name"];
    [coder encodeObject:self.t_boosterpoints forKey:@"t_boosterpoints"];
    [coder encodeObject:self.sp_logo forKey:@"sp_logo"];
    // all other properties
}

#pragma mark - Save Response Data In NSUserDefault

- (void)saveCustomObject:(ModelClass *)object key:(NSString *)key
{
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    [NSUserDefaults saveObject:encodedObject forKey:key];
}


@end
