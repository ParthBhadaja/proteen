//
//  ModelClass.h
//  ProTeen
//
//  Created by Apple 1 on 28/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PT_ImportClasses.h"

@interface ModelClass : NSObject

@property(strong,nonatomic)NSString *sp_bank_name;
@property(strong,nonatomic)NSString *sp_credit;
@property(strong,nonatomic)NSString *t_pincode;
@property(strong,nonatomic)NSString *ID;
@property(strong,nonatomic)NSString *t_device_type;
@property(strong,nonatomic)NSString *t_country;
@property(strong,nonatomic)NSString *created_at;
@property(strong,nonatomic)NSString *t_gender;
@property(strong,nonatomic)NSString *sp_bank_accountno;
@property(strong,nonatomic)NSString *t_school;
@property(strong,nonatomic)NSString *c_name;
@property(strong,nonatomic)NSString *sp_email;
@property(strong,nonatomic)NSString *country_id;
@property(strong,nonatomic)NSString *t_social_identifier;
@property(strong,nonatomic)NSString *t_birthdate;
@property(strong,nonatomic)NSString *t_email;
@property(strong,nonatomic)NSString *t_location;
@property(strong,nonatomic)NSString *c_code;
@property(strong,nonatomic)NSString *t_level;
@property(strong,nonatomic)NSString *t_uniqueid;
@property(strong,nonatomic)NSString *t_sponsor_choice;
@property(strong,nonatomic)NSString *sp_isapproved;
@property(strong,nonatomic)NSString *sponser_id;
@property(strong,nonatomic)NSString *t_isverified;
@property(strong,nonatomic)NSString *teenager_id;
@property(strong,nonatomic)NSString *remember_token;
@property(strong,nonatomic)NSString *t_social_provider;
@property(strong,nonatomic)NSString *t_photo;
@property(strong,nonatomic)NSString *deleted;
@property(strong,nonatomic)NSString *t_credit;
@property(strong,nonatomic)NSString *updated_at;
@property(strong,nonatomic)NSString *sp_company_name;
@property(strong,nonatomic)NSString *sp_admin_name;
@property(strong,nonatomic)NSString *t_phone;
@property(strong,nonatomic)NSString *t_isfirstlogin;
@property(strong,nonatomic)NSString *t_device_token;
@property(strong,nonatomic)NSString *password;
@property(strong,nonatomic)NSString *t_nickname;
@property(strong,nonatomic)NSString *t_name;
@property(strong,nonatomic)NSString *t_boosterpoints;
@property(strong,nonatomic)NSString *sp_logo;

-(id)initwithDictionary : (id)dict;
- (void)saveCustomObject:(ModelClass *)object key:(NSString *)key;

@end
