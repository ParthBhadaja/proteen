//
//  ProfileViewController.m
//  ProTeen
//
//  Created by Apple 1 on 09/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "ProfileViewController.h"
#import "PT_GlobalMethods.h"
#import "PT_ImportClasses.h"

@interface ProfileViewController ()<UITextFieldDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,imagePickingDelegate,datePickerDelegate>
{
    /*----- Textfield -----*/
    
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtCountry;
    
    /*----- UIButton -----*/
    
    IBOutlet UIButton *btnUpdate;
    IBOutlet UIButton *btnBirthDate;
    
    /*----- UIImageView -----*/
    
    IBOutlet UIImageView *imgViewObj;
    
    /*----- UIImageView -----*/
    
    IBOutlet TPKeyboardAvoidingScrollView *scrollViewObj;
    
    /*----- UIGesture -----*/
    
    UITapGestureRecognizer *tapGestureObj;
    
    
    /*----- UIAlertController -----*/
    
    UIAlertController *alertController;
    
    /*----- UIImagePickerController -----*/
    
    UIImagePickerController *imgPickerControllerObj;
    
    /*----- PT_GlobalMethods -------*/
    
    PT_GlobalMethods *globalMethodClassObj;
}

-(IBAction)btnUpdate:(id)sender;

@end

@implementation ProfileViewController
@synthesize imProfileObj;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"%@",kAppDel.modelClassObj.t_nickname);
    
    /*----- Set Textfield Properties -----*/
    txtName.autocapitalizationType=UITextAutocapitalizationTypeWords;
    txtPassword.secureTextEntry=YES;
    [self setTextfieldDelegate];
    
    /*----- Set Gesture on ImageView -----*/
    
    tapGestureObj = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(selectImage:)];
    tapGestureObj.delegate=self;
    tapGestureObj.numberOfTapsRequired=1;
    [imgViewObj addGestureRecognizer:tapGestureObj];
    imgViewObj.userInteractionEnabled=TRUE;
    
    
    /*------ Image Picker Controller Allocation ------*/
    
    imgPickerControllerObj = [[UIImagePickerController alloc]init];
    imgPickerControllerObj.delegate=self;
    imgPickerControllerObj.allowsEditing=YES;
    imgViewObj.clipsToBounds=YES;
    imgViewObj.layer.cornerRadius=45;
    
    /*------ Set Value on Textfield ------*/
    
    imgViewObj.image=self.imProfileObj;
    
    /*------ gloabal Method Class Allocation -------*/
    
    globalMethodClassObj = [[PT_GlobalMethods alloc]init];
    
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Textfield Delegate Methods


- (void)textFieldDidEndEditing:(UITextField *)tekuxtField
{
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (![PT_GlobalMethods checkSpaceAndEmojiOnTextfield:textField])
    {
        return NO;
    }
    
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == txtName)
    {
        [txtName resignFirstResponder];
        [txtEmail becomeFirstResponder];
    }
    if (textField == txtEmail)
    {
        [txtEmail resignFirstResponder];
        [txtPassword becomeFirstResponder];
    }
    else if (textField == txtPassword)
    {
        [txtPassword resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - TextField Validation Method

- (void)checkRequieredFiledwithEmail:(NSString*)strEmail withPassword:(NSString*)strPassword
{
    if ([[strEmail RemoveNullFromString:YES] length] > 0 && [strPassword length] > 0)
    {
        [PT_GlobalMethods setButtonEnabled:btnUpdate];
    }
    else
    {
        [PT_GlobalMethods setButtonDisabled:btnUpdate];
    }
}

#pragma mark - ImagePicker Controller Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    imgViewObj.image = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - All Actions


-(IBAction)btnUpdate:(id)sender
{
    if ([PT_GlobalMethods checkEmailValidation:txtEmail.text])
    {
        /*------ Parameter Dictionary ------*/
        NSMutableDictionary *dict = [NSMutableDictionary new];
        [dict setObject:@"" forKey:@""];
        
        /*-------- Call Webservice ----------*/
        [PT_WebService callPostWithURLStringwithParameters:dict withViewCtr:[PT_GlobalMethods topViewController] withCompletionHandler:^(NSURLSessionDataTask *task, id responseObject, NSError *error)
         {
             
             if (responseObject != nil)
             {
                [PT_GlobalMethods setTextfieldEmpty:scrollViewObj];
                 [PT_GlobalMethods showAlertwithTitle:@"Alert!" withMessage:@"SignUp Successfully" withViewController:[PT_GlobalMethods topViewController]];
             }
             else
             {
                 
             }
             
         }];
    }
    else
    {
        [PT_GlobalMethods showAlertwithTitle:@"Alert!" withMessage:@"Please Enter Valid Email" withViewController:[PT_GlobalMethods topViewController]];
        
    }
}
#pragma mark - Set Textfields Delegate

-(void)setTextfieldDelegate
{
    for (UITextField *txt in scrollViewObj.subviews)
    {
        if ([txt isKindOfClass:[UITextField class]])
        {
            txt.delegate=self;
            txt.clearButtonMode=UITextFieldViewModeWhileEditing;
        }
    }
}

#pragma mark - Other Methods

/*------  Set Button Enable And Disable ---------*/

-(void)setSignUpButtonDisable
{
    [btnUpdate setEnabled:FALSE];
    [btnUpdate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
}

-(void)setSignUpButtonEnable
{
    [btnUpdate setEnabled:TRUE];
    [btnUpdate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


/*------- Gesture Method ----------*/

-(void)selectImage:(UIGestureRecognizer *)gesture
{
    
    globalMethodClassObj.delegate=self;
    [globalMethodClassObj OpenAlertForImagePicker:imgPickerControllerObj ViewController:[PT_GlobalMethods topViewController] navigationViewController:self.navigationController];
    
    /*
     dispatch_async(dispatch_get_main_queue(), ^ {
     [self presentViewController:alertController animated:YES completion:nil];
     });
     */
    
}


#pragma mark - Get DatePicker Value Via Gloabal Method

-(void)returnDate :(NSString *)strDate
{
//    txtBirthDate.text = strDate;
}


#pragma mark - Get Image Via Gloabal Method

-(void)returnImage :(UIImage *)img
{
    imgViewObj.image=img;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
