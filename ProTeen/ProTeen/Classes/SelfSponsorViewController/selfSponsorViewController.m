//
//  selfSponsorViewController.m
//  ProTeen
//
//  Created by Apple on 23/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "selfSponsorViewController.h"

@interface selfSponsorViewController ()

@end

@implementation selfSponsorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self view] setFrame:[[UIScreen mainScreen] bounds]];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnDoneClick:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.alpha=0.0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}
@end
