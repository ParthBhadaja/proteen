//
//  SplashViewController.m
//  ProTeen
//
//  Created by Apple 1 on 14/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import "SplashViewController.h"
#import "LoginViewController.h"
#import "PT_ImportClasses.h"



@interface SplashViewController ()
{
    IBOutlet UIImageView *imgViewLogo;
    IBOutlet UIImageView *imgViewLogo2;
}
@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view layoutIfNeeded];
    
    _leftLogoX.constant = -(SYSTEM_SCREEN_SIZE.width/2)-100;
    _rightLogoX.constant = (SYSTEM_SCREEN_SIZE.width/2)+100;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self SlideAnimation]; // initial call Slide Animation Method
    
   // Do any additional setup after loading the view from its nib.
}

/*------ Slide Left And Right Animation ------*/

-(void)SlideAnimation
{
    
    [UIView animateWithDuration:2.0 animations:^{
        _leftLogoX.constant = 0;
        _rightLogoX.constant = 0;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self ZoomInOutAnimation];
    }];
    
}

/*------ Zoom IN OUT Animation ------*/

-(void)ZoomInOutAnimation
{
    [CATransaction begin];
    CABasicAnimation *scaleInOut = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleInOut.duration = .20;
    scaleInOut.autoreverses = YES;
    scaleInOut.fromValue = [NSNumber numberWithFloat:1.0];
    scaleInOut.toValue = [NSNumber numberWithFloat:1.1];
    scaleInOut.repeatCount = 0;
    scaleInOut.fillMode = kCAFillModeBoth;
    
    [CATransaction setCompletionBlock:^{
        [self performSelector:@selector(dismiss) withObject:nil afterDelay:.10];
    }];
    // Now we attach the animation.
    [imgViewLogo.layer addAnimation:scaleInOut forKey:nil];
    [imgViewLogo2.layer addAnimation:scaleInOut forKey:nil];
    [CATransaction commit];
}



/*------ Push ViewController With Animation ------*/
-(void)dismiss
{
    LoginViewController *loginViewControllerObj =[[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    loginViewControllerObj.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [self.navigationController pushViewController:loginViewControllerObj animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
