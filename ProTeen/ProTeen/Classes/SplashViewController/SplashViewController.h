//
//  SplashViewController.h
//  ProTeen
//
//  Created by Apple 1 on 14/03/16.
//  Copyright © 2016 iNexture. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leftLogoX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rightLogoX;
@end
